<?php 
session_start(); 
include("includes/inc_dbc.php");

$_SESSION['pagename'] = "Teachers";
if(isset($_SESSION['user']))
{
	if($_SESSION['role']=='admin')
	{
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo isset($_SESSION['pagename'])?$_SESSION['pagename']." | ":""; ?>E-Xams</title>
	<?php include("includes/inc_styles.php"); ?>
</head>
<body>

<div class="jumbotron">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<?php include('includes/inc_logo.php'); ?>
			</div>
			<div class="col-md-6 text-right">
				<?php include('includes/inc_logout.php'); ?>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<?php
			if(isset($_SESSION['error']))
			{	
				echo "<div class='col-md-12'>";
				echo "<p class='alert alert-danger' id='msg-err'>".$_SESSION['error']."</p>";
				echo "</div>";
				unset($_SESSION['error']);
			}
			else if (isset($_SESSION['success'])) 
			{
				echo "<div class='col-md-12'>";
				echo "<p class='alert alert-success' id='msg-succ'>".$_SESSION['success']."</p>";
				echo "</div>";
				unset($_SESSION['success']);
			}
		?>
	</div>
	<div class="row">
		<div class="col-md-3">
			<div class="well">
				<?php include("includes/inc_navs.php"); ?>
			</div>
		</div>
		<div class="col-md-9">
			<?php include("includes/inc_teachersContent.php"); ?>
		</div>	
	</div>
	<?php include("includes/inc_footer.php"); ?>
</div>
</body>
</html>
<?php
	}
	else
	{
		header("Location: userIndex.php");
	}
}
else
{
	$_SESSION['error'] = "Please login to continue.";
	header("Location: index.php");
}
?>