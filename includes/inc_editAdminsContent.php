<?php

if(isset($_GET['aid']) AND !empty($_GET['aid']))
{
	$sql = "SELECT * FROM tbl_users_admins AS a JOIN tbl_users AS u ON a.uid=u.uid WHERE a.aid=".$_GET['aid'];

	$run = mysqli_query($dbc,$sql);
	$count = mysqli_num_rows($run);
	if($count>0)
	{
		$row = mysqli_fetch_array($run);
?>

<form action="controller.php" method="POST" class="form" enctype="multipart/form-data">
	<input type="hidden" name="aid" value="<?php echo $row["aid"]; ?>" />
	<input type="hidden" name="uid" value="<?php echo $row["uid"]; ?>" />
	<div class="row">
		<div class="col-md-12">
			<h3>Personal</h3>
		</div>
		<div class="col-md-3">
			<input type="text" name="afname" placeholder="First Name" class="form-control" value="<?php echo $row['fname']; ?>" required />
		</div>
		<div class="col-md-3">
			<input type="text" name="alname" placeholder="Last Name" class="form-control" value="<?php echo $row['lname']; ?>" required />
		</div>
		<div class="col-md-2">
			<select class="form-control" name="agender" required>
				<option value="" disabled>Gender</option>
				<option value="Male" <?php echo ($row['gender']=="Male")?"selected":""; ?> >Male</option>
				<option value="Female" <?php echo ($row['gender']=="Female")?"selected":""; ?> >Female</option>
			</select>
		</div>
		<div class="col-md-1 img-modal">
			<a href="#" data-toggle="modal" data-target="#proimage"><img src="<?php echo $row['propic']; ?>" class="img-cover" /></a>
			<div class="modal fade" id="proimage" role="dialog">
				<div class="modal-dialog">
					<img src="<?php echo $row['propic']; ?>" class="img-responsive" />
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<label class="form-control btn btn-info btn-file">
				<input type="file" name="apropic" placeholder="Your Photo" class="form-control" onchange="fchanges(this);"><span class="small">Upload Picture</span>
			</label>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<h3>Contact</h3>
		</div>
		<div class="col-md-4">
			<input type="text" name="aaddress" placeholder="Address" class="form-control" value="<?php echo $row['address']; ?>" required>
		</div>
		<div class="col-md-4">
			<input type="number" name="aphone" placeholder="Phone Number" class="form-control" value="<?php echo $row['phone']; ?>" required data-validation="length" data-validation-length="max13">
		</div>
		<div class="col-md-4">
			<input type="email" name="aemail" placeholder="Email Address" class="form-control" value="<?php echo $row['email']; ?>" required>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<h3>Administrative</h3>
		</div>
		<div class="col-md-4">
			<input type="text" name="afaculty" placeholder="Faculty" class="form-control" value="<?php echo $row['faculty']; ?>" required>
		</div>
		<div class="col-md-4">
			<input type="text" name="apost" placeholder="Post" class="form-control" value="<?php echo $row['post']; ?>" required>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<h3>Account</h3>
		</div>
		<div class="col-md-4">
			<input type="text" name="ausername" placeholder="Login Username" class="form-control" value="<?php echo $row['username']; ?>" required>
		</div>
		<div class="col-md-4">
			<input type="password" name="apassword" placeholder="Login Password" class="form-control" required data-validation="length" data-validation-length="min6" data-validation-optional="true" data-validation-error-msg="The password must be at least 6 characters." value="<?php echo $row['password']; ?>">
		</div>
		<div class="col-md-4">
			<input type="password" name="acpassword" placeholder="Confirm Login Password" class="form-control" required data-validation="confirmation" data-validation-confirm="apassword" data-validation-error-msg="The passwords in the two fields do not match." value="<?php echo $row['password']; ?>">
		</div>
	</div>


	<div class="row">
		<div class="col-md-12 text-right">
			<input type="hidden" name="editAdmin" value="1" />
			<button class="btn btn-lg btn-success">Update Admin</button>
		</div>
	</div>
</form>

<script type="text/javascript">
	
	$(document).ready(function(){
		$.validate({
    		modules : 'security'
		});

	localStorage.setItem("origImgSrc",$(".img-modal img").attr("src"));
	});

	function previewImg(input) {
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();

	        reader.onload = function (e) {
	        	var imgSrc = e.target.result;
	        	$(input).closest(".row").find(".img-modal").find("img").attr("src",imgSrc);
	        	$(input).closest(".row").find(".img-modal .modal img").attr("src",imgSrc);
	        }

	        reader.readAsDataURL(input.files[0]);
	    }
	}

	function fchanges(temp) {
		var input = $(temp);
		if(input.val()=="")
		{
	        input.siblings("span").html("Upload Picture");
			input.closest(".btn-file").addClass("btn-info");
			input.closest(".btn-file").removeClass("btn-success");
	        input.closest(".row").find(".img-modal img").attr("src",localStorage.getItem("origImgSrc"));
		}
		else
		{
	    	var filePath = input.val();
			var filePathArr = filePath.split("\\");
			var filePath = filePathArr[filePathArr.length-1];
			input.siblings("span").html(filePath);
			input.closest(".btn-file").addClass("btn-success");
			input.closest(".btn-file").removeClass("btn-info");
			// input.closest(".row").find(".img-modal").remove();
	    	previewImg(temp);
		}
	}
</script>
<?php
	}
	else
	{
		$_SESSION['error'] = "The specified admin does not exist.";
		header("Location: admins.php");
	}
}
else
{
	$_SESSION['error'] = "Admin ID must be specified.";
	header("Location: admins.php");
}
?>