<link href="https://fonts.googleapis.com/css?family=Pacifico|Raleway|Josefin+Sans|Rajdhani|Spicy+Rice" rel="stylesheet">

<script src="scripts/jquery.min.js"></script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="scripts/jquery-ui.js"></script>

<script src="scripts/form-validator/jquery.form-validator.min.js"></script>
 
<link href="styles/theme-default.min.css"
    rel="stylesheet" type="text/css" />

<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
<script src="bootstrap/js/bootstrap.js"></script>

<script src="scripts/bootbox.min.js"></script>

<link rel="stylesheet" type="text/css" href="styles/mainstyle.css">

<link rel="icon" href="images/exams_favicon.ico" type="image/ico" sizes="32x32">