<form action="controller.php" method="POST" class="form" enctype="multipart/form-data">
	<div class="row">
		<div class="col-md-12">
			<h3>Personal</h3>
		</div>
		<div class="col-md-3">
			<input type="text" name="afname" placeholder="First Name" class="form-control" required />
		</div>
		<div class="col-md-3">
			<input type="text" name="alname" placeholder="Last Name" class="form-control" required />
		</div>
		<div class="col-md-2">
			<select class="form-control" name="agender" required>
				<option value="" disabled selected>Gender</option>
				<option value="Male">Male</option>
				<option value="Female">Female</option>
			</select>
		</div>
		<div class="col-md-1">
			&nbsp;
		</div>
		<div class="col-md-3">
			<label class="form-control btn btn-info btn-file">
				<input type="file" name="apropic" placeholder="Your Photo" class="form-control" onchange="fchanges(this);" required><span class="small">Upload Picture</span>
			</label>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<h3>Contact</h3>
		</div>
		<div class="col-md-4">
			<input type="text" name="aaddress" placeholder="Address" class="form-control" required>
		</div>
		<div class="col-md-4">
			<input type="number" name="aphone" placeholder="Phone Number" class="form-control" required data-validation="length" data-validation-length="max13">
		</div>
		<div class="col-md-4">
			<input type="email" name="aemail" placeholder="Email Address" class="form-control" required>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<h3>Administrative</h3>
		</div>
		<div class="col-md-4">
			<input type="text" name="afaculty" placeholder="Faculty" class="form-control" required>
		</div>
		<div class="col-md-4">
			<input type="text" name="apost" placeholder="Post" class="form-control" required>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<h3>Account</h3>
		</div>
		<div class="col-md-4">
			<input type="text" name="ausername" placeholder="Login Username" class="form-control" required>
		</div>
		<div class="col-md-4">
			<input type="password" name="apassword" placeholder="Login Password" class="form-control" required data-validation="length" data-validation-length="min6" data-validation-optional="true" data-validation-error-msg="The password must be at least 6 characters.">
		</div>
		<div class="col-md-4">
			<input type="password" name="acpassword" placeholder="Confirm Login Password" class="form-control" required data-validation="confirmation" data-validation-confirm="apassword" data-validation-error-msg="The passwords in the two fields do not match.">
		</div>
	</div>


	<div class="row">
		<div class="col-md-12 text-right">
			<input type="hidden" name="addAdmin" value="1" />
			<button class="btn btn-lg btn-success">Add Admin</button>
		</div>
	</div>
</form>

<script type="text/javascript">

	$(document).ready(function(){
		$.validate({
    		modules : 'security'
		});
	});

	function previewImg(input) {
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();

	        reader.onload = function (e) {
	        	var imgfile = '<div class="img-modal"> \
	        	<a href="#" data-toggle="modal" data-target="#proimage"><img src="'+ e.target.result +'" class="img-cover" /></a> \
	        				<div class="modal fade" id="proimage" role="dialog"> \
	        					<div class="modal-dialog"> \
	        						<img src="'+ e.target.result +'" class="img-responsive" /> \
	        					</div> \
	        				</div> \
	        			</div>';

	        	$(input).closest(".row").find(".col-md-1").prepend(imgfile);
	        }

	        reader.readAsDataURL(input.files[0]);
	    }
	}

	function fchanges(temp) {
		var input = $(temp);
		if(input.val()=="")
		{
	        input.siblings("span").html("Upload Picture");
			input.closest(".btn-file").addClass("btn-info");
			input.closest(".btn-file").removeClass("btn-success");
	        input.closest(".row").find(".img-modal").remove();
		}
		else
		{
	    	var filePath = input.val();
			var filePathArr = filePath.split("\\");
			var filePath = filePathArr[filePathArr.length-1];
			input.siblings("span").html(filePath);
			input.closest(".btn-file").addClass("btn-success");
			input.closest(".btn-file").removeClass("btn-info");
			input.closest(".row").find(".img-modal").remove();
	    	previewImg(temp);
		}
	}
</script>