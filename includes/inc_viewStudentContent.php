<?php
$sid = $_REQUEST['sid'];
$sqls = "SELECT * FROM tbl_users_students WHERE sid=".$sid;
$runs = mysqli_query($dbc,$sqls);
$row = mysqli_fetch_array($runs);

?>
<div class="row">
	<div class="col-md-10">
	<?php

	echo "<h2 class='role-name'>Student</h2>
		  <h1>".$row['fname']." ".$row['lname']."</h1>
		  <h3>".$row['gender']."</h3>
		  <h3>".$row['email']."</h3>
		  <h3>".$row['address']."</h3>
		  <h3>".$row['phone']."</h3>
		  <h3>Graduated Level: ".$row['grad_level']."</h3>
		  <h3>Graduated Year: ".$row['grad_year']."</h3>
		  <h3>Applying For: ";
	echo ($row['apply_for']!="")?($row['apply_for']):($row['attended_exam']." (Exam Attended)");
	echo "</h3>	
		  <h3>Score: ".$row['score']."</h3>";

	?>
	</div>
	<?php
		$base_dir = "http://localhost/e-xams/";
		$img = ($row['propic']=="")?($base_dir."images/exams_.png"):($row['propic']);
	?>
	<div class="col-md-2 img-modal">
		<a href="#" data-toggle="modal" data-target="#proimage"><img src="<?php echo $img; ?>" class="img-responsive" id="pro-pic" /></a>
		<div class="modal fade" id="proimage" role="dialog">
			<div class="modal-dialog">
				<img src="<?php echo $img; ?>" class="img-responsive" />
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-1 col-md-offset-10"><a href='<?php echo "editStudents.php?sid=".$row['sid']."'"; ?>' class='btn btn-sm btn-info glyphicon glyphicon-pencil'></a></div>
	<div class="col-md-1"><a href='<?php echo "controller.php?deleteThisStudent=".$row['sid']."'"; ?>' class='btn btn-sm btn-danger glyphicon glyphicon-remove'></a></div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$("a.glyphicon-remove").on("click",function(e){
			e.preventDefault();
			var togo = $(this).attr("href");
			bootbox.confirm({
			    message: "Are you sure you want to delete this student?",
			    buttons: {
			        confirm: {
			            label: 'Yes',
			            className: 'btn-success'
			        },
			        cancel: {
			            label: 'No',
			            className: 'btn-danger'
			        }
			    },
			    callback: function (result) {
// window.location.href = "http://stackoverflow.com";
			        if(result)
			        {
						window.location.href = togo;
			        }
			    }
			});
		})
	});
</script>