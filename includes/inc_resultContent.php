<?php
if($_SESSION['role']=="admin" or $_SESSION['role']=="teacher")
{
	if(isset($_GET['sid']) AND $_GET['sid']!="")
	{
		$sqls = "SELECT sid, fname, lname FROM tbl_users_students WHERE sid=".$_GET['sid'];
		$runs = mysqli_query($dbc,$sqls);
		$counts = mysqli_num_rows($runs);

		if($counts>0)
		{
			$rows = mysqli_fetch_array($runs);
			echo "<h2>Student Result</h2>";
			echo "<h3>".$rows['fname']." ".$rows['lname']." (id:".$rows['sid'].")"."</h3>";
		

			$sqlr = "SELECT * FROM tbl_results AS r JOIN tbl_exams AS e ON r.eid=e.eid WHERE sid=".$_GET['sid']." ORDER BY r.rid desc";
			$runr = mysqli_query($dbc,$sqlr);
			$countr = mysqli_num_rows($runr);
			$r = 1;
			
			if($countr)
			{
?>

<div class="row">
	<div class="col-md-12">
		<table class="table table-hover table-bordered table-condensed">
			<thead>
				<tr>
					<th>S. No.</th>
					<th>Exam Code</th>
					<th>Questions</th>
					<th>Unanswered</th>
					<th>Wrong</th>
					<th>Correct</th>
					<th>Score</th>
					<th>Time Taken</th>
				</tr>
			</thead>
			<tbody>
			<?php
				while($rowr = mysqli_fetch_array($runr))
				{
					$sec = $rowr['timetaken'];
					$hour = (int)($sec / (60 * 60));
					$sec -= $hour * (60 * 60);
					$min = (int)($sec / 60);
					$sec -= $min * 60;
					echo "
					<tr>
						<td>$r</td>
						<td><a href='results.php?eid=".$rowr['eid']."' title='View Exam Results'>".$rowr['ecode']."</a></td>
						<td>".$rowr['total_questions']."</td>
						<td>".$rowr['unanswered']."</td>
						<td>".$rowr['wrong']."</td>
						<td>".$rowr['correct']."</td>
						<td>".round(($rowr['correct']/$rowr['total_questions'])*(100),2)."%</td>
						<td>".$hour."h ".$min."m ".$sec."s</td>
					</tr>
					";
					$r++;
				}
			?>
			</tbody>
		</table>
	</div>
</div>

<?php
			}
			else
			{
				echo "No Results Found.";
			}
		}
		else
		{
			header("Location: students.php");
		}
	}

	else if(isset($_GET['eid']) AND $_GET['eid']!="")
	{
		$sqle = "SELECT * FROM tbl_exams WHERE eid=".$_GET['eid'];
		$rune = mysqli_query($dbc,$sqle);
		$counte = mysqli_num_rows($rune);

		if($counte>0)
		{
			$rowe = mysqli_fetch_array($rune);
			$sece = $rowe['eduration'];
			$houre = (int)($sece / (60 * 60));
			$sece -= $houre * (60 * 60);
			$mine = (int)($sece / 60);
			$sece -= $mine * 60;

			echo "<h2>Exam Result</h2>";
			echo "<h3>Code: ".$rowe['ecode']."</h3>
				  <h3>Name: ".$rowe['ename']."</h3>
				  <h3>Type: ".$rowe['etype']."</h3>
				  <h3>Date: ".$rowe['edate']."</h3>
				  <h3>Time: ".$rowe['etime']."</h3>
				  <h3>Duration: ".$houre."h ".$mine."m ".$sece."s</h3>";

			$sqlr = "SELECT * FROM tbl_results AS r JOIN tbl_users_students AS s ON r.sid=s.sid WHERE r.eid=".$_GET['eid']." ORDER BY r.correct desc";
			$runr = mysqli_query($dbc,$sqlr);
			$countr = mysqli_num_rows($runr);
			$r = 1;
			
			if($countr)
			{
?>

<div class="row">
	<div class="col-md-12">
		<table class="table table-hover table-bordered table-condensed">
			<thead>
				<tr>
					<th>S. No.</th>
					<th>Student Name</th>
					<th>Total</th>
					<th>Unanswered</th>
					<th>Wrong</th>
					<th>Correct</th>
					<th>Score</th>
					<th>Time Taken</th>
				</tr>
			</thead>
			<tbody>
			<?php
				while($rowr = mysqli_fetch_array($runr))
				{
					$sec = $rowr['timetaken'];
					$hour = (int)($sec / (60 * 60));
					$sec -= $hour * (60 * 60);
					$min = (int)($sec / 60);
					$sec -= $min * 60;
					echo "
					<tr>
						<td>$r</td>
						<td><a href='viewStudent.php?sid=".$rowr['sid']."' title='View Student'>".$rowr['fname']." ".$rowr['lname']."</a></td>
						<td>".$rowr['total_questions']."</td>
						<td>".$rowr['unanswered']."</td>
						<td>".$rowr['wrong']."</td>
						<td><a href='results.php?sid=".$rowr['sid']."' title='View Student Results'>".$rowr['correct']."</td>
						<td>".round(($rowr['correct']/$rowr['total_questions'])*(100),2)."%</td>
						<td>".$hour."h ".$min."m ".$sec."s</td>
					</tr>
					";
					$r++;
				}
			?>
			</tbody>
		</table>
	</div>
</div>

<?php
			}
			else
			{
				echo "No Results Found.";
			}
		}
		else
		{
			header("Location: exams.php");
		}
	}

	else
	{
		header("Location: students.php");
	}
}
else
{
	$sqlr = "SELECT * FROM tbl_results AS r JOIN tbl_exams AS e ON r.eid=e.eid WHERE sid=".$row['sid']." ORDER BY r.rid desc";
	$runr = mysqli_query($dbc,$sqlr);
	$countr = mysqli_num_rows($runr);
	$r = 1;
	
	if($countr)
	{
?>

<div class="row">
	<div class="col-md-12">
		<table class="table table-hover table-bordered table-condensed">
			<thead>
				<tr>
					<th>S. No.</th>
					<th>Exam Code</th>
					<th>Questions</th>
					<th>Unanswered</th>
					<th>Wrong</th>
					<th>Correct</th>
					<th>Score</th>
					<th>Time Taken</th>
				</tr>
			</thead>
			<tbody>
			<?php
			while($rowr = mysqli_fetch_array($runr))
			{
				$sec = $rowr['timetaken'];
				$hour = (int)($sec / (60 * 60));
				$sec -= $hour * (60 * 60);
				$min = (int)($sec / 60);
				$sec -= $min * 60;
				echo "
				<tr>
					<td>$r</td>
					<td>".$rowr['ecode']."</td>
					<td>".$rowr['total_questions']."</td>
					<td>".$rowr['unanswered']."</td>
					<td>".$rowr['wrong']."</td>
					<td>".$rowr['correct']."</td>
					<td>".round(($rowr['correct']/$rowr['total_questions'])*(100),2)."%</td>
					<td>".$hour."h ".$min."m ".$sec."s</td>
				</tr>
				";
				$r++;
			}
			?>
			</tbody>
		</table>
	</div>
</div>

<?php
	}
	else
	{
		echo "No Results Found.";
	}
}
?>