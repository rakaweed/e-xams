<form method="POST" enctype="multipart/form-data" action="examController.php" id="examForm">
<?php

// $sqlq = "SELECT q.*, e.ecode FROM tbl_questions AS q JOIN tbl_exams AS e ON q.eid=e.eid WHERE e.ecode='".$row['apply_for']."' ORDER BY q.category, q.qorder, q.qid";

$sqlq = "SELECT q.*, e.ecode FROM tbl_questions AS q JOIN tbl_exams AS e ON q.eid=e.eid WHERE e.ecode='".$row['apply_for']."' ORDER BY q.category, rand()";
$runq = mysqli_query($dbc,$sqlq);
$countq = mysqli_num_rows($runq);

if($countq>0)
{
?>
	<div class="row">
		<div class="col-md-12">Welcome <?php echo $row['fname']; ?>. Answer the following questions withing the allocated time, and then click Submit. If you fail to click Submit before the allocated time, your completed answers will be auto-submitted. </div>
	</div>
	<div class="row">
	<?php include("scripts/scr_countdown.php"); ?>
		<div class="col-md-6 text-center">
			<div class="panel panel-info">
				<div class="panel-heading text-center">Time Left</div>
				<div class="panel-body text-center" id="countdown">&nbsp;</div>
			</div>
		</div>
		<div class="col-md-6 text-center">
			<div class="panel panel-info">
				<div class="panel-heading text-center">Time Elapsed</div>
				<div class="panel-body text-center" id="countdownTt">&nbsp;</div>
			</div>
		</div>
	</div>
<?php

	$q = 1;
	while ($rowq = mysqli_fetch_array($runq))
	{
		$e = $rowq['eid'];
		$c = 'a';
?>
	<div class="row">
		<div class="col-md-12">
			<h3><?php echo $q.". ".$rowq['question']; $q++; ?></h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-11 col-md-offset-1">
			<div class="radio">
				<input type="radio" name="<?php echo $rowq['qid']; ?>" value="<?php echo $c; $c++; ?>"><h4><?php echo $rowq['choice_a']; ?></h4>
				<input type="radio" name="<?php echo $rowq['qid']; ?>" value="<?php echo $c; $c++; ?>"><h4><?php echo $rowq['choice_b']; ?></h4>
				<input type="radio" name="<?php echo $rowq['qid']; ?>" value="<?php echo $c; $c++; ?>"><h4><?php echo $rowq['choice_c']; ?></h4>
				<input type="radio" name="<?php echo $rowq['qid']; ?>" value="<?php echo $c; $c++; ?>"><h4><?php echo $rowq['choice_d']; ?></h4>
			</div>
		</div>
	</div>
	<?php
		}
	?>
	<div class="row">
		<div class="col-md-12 text-center">
			<input type="hidden" name="submitExamAnswers" value="<?php echo $e.'&'.$row['sid'].'&'.($q-1); ?>" />
			<span id="timeTaken"></span>
			<button class="btn btn-primary btn-submit">Submit</button>
		</div>
	</div>
</form>

<?php
	}
	else
	{
		$_SESSION['error'] = "You cannot attend the exam because of either of the following:<br>
		You have not been assigned to any exam.<br>
		The exam you're attending doesn't exist.<br>
		You have already attended the exam.";
		header('Location: userIndex.php');
	}
?>

<script type="text/javascript">
	$(document).ready(function(){
		$("button.btn-submit").on("click",function(e){
			e.preventDefault();
			bootbox.confirm({
			    message: "Are you sure you have checked and confirmed all your answers?",
			    buttons: {
			        confirm: {
			            label: 'Yes',
			            className: 'btn-success'
			        },
			        cancel: {
			            label: 'No',
			            className: 'btn-danger'
			        }
			    },
			    callback: function (result) {
			        if(result)
			        {
			        	$("form#examForm").submit();
			        }
			    }
			});
		});
	});
</script>