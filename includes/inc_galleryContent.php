<?php

if(isset($_GET['viewall']))
	$sqli = "SELECT * FROM tbl_gallery ORDER BY appear_order";
else
	$sqli = "SELECT * FROM tbl_gallery WHERE status='active' ORDER BY appear_order";

$runi = mysqli_query($dbc,$sqli);
$counti = mysqli_num_rows($runi);
$i = 0;

?>
<div class="row">
	<div class="col-md-12 text-right">
		<?php 
			echo isset($_GET['viewall'])?'<a href="?viewactive" class="btn btn-sm btn-default glyphicon glyphicon-eye-close" title="View Active Only"></a>':'<a href="?viewall" class="btn btn-sm btn-default glyphicon glyphicon-eye-open" title="View All"></a>'; 
		?>
	</div>
</div>

<form action="controller.php" method="POST">
<div class="row">
	<div class="col-md-12">
		<div class="sortable">
		<?php
		if($counti>0)
		{
			while ($rowi = mysqli_fetch_array($runi))
			{
			?>
			<div class='well ui-state-default <?php echo ($rowi["status"]!="active")?"bg-danger":""; ?>' id='well_<?php echo $i; ?>'>
		    	<div class='row'>
		    		<div class='col-md-12 text-right'>
						<a href='controller.php?deleteThisimgid=<?php echo $rowi["imgid"]; ?>' class='btn btn-xs btn-danger glyphicon glyphicon-trash'><span>Remove</span></a>
		    		</div>
		    	</div>
			    <div class='row'>
					<div class='col-md-3'>
						<a href="#" data-toggle="modal" data-target="#proimage_<?php echo $i; ?>"><img src="uploads/img_gallery/<?php echo $rowi['location']; ?>" class="img-responsive img-rounded" /></a>
						<div class="modal fade" id="proimage_<?php echo $i; ?>" role="dialog">
        					<div class="modal-dialog">
        						<img src="uploads/img_gallery/<?php echo $rowi['location']; ?>" class="img-responsive" />
        					</div>
        				</div> 
					</div>
					<div class='col-md-5'>
						<h3><?php echo $rowi['img_name']; ?></h3>						
						<input type="hidden" name="imgid[]" value="<?php echo $rowi['imgid']; ?>" />
					</div>
					<div class="col-md-4 text-right">
						<?php
						echo ($rowi['status']=='active')?'<a href="controller.php?changestatusto=inactive&img_id='.$rowi['imgid'].'" class="btn btn-sm btn-warning glyphicon glyphicon-eye-close"><span>Deactivate</span></a>':'<a href="controller.php?changestatusto=active&img_id='.$rowi['imgid'].'" class="btn btn-sm btn-primary glyphicon glyphicon-eye-open"><span>Activate</span></a>';
						?> 
					</div>
				</div>
			</div>
			<?php
				$i++;
			}
		}
		?>	
		</div>
	</div>
	<div class="col-md-12 text-right">
		<input type="hidden" name="saveImgOrder" value="1">
		<button class="btn btn-sm btn-success glyphicon glyphicon-floppy-disk" title="Save Image Order"><span>Save Image Order</span></button>
	</div>
</div>	
</form>

<hr>
<h2>Add More</h2>
<hr>

<form action="controller.php" method="POST" enctype="multipart/form-data">
<input type="hidden" name="radioCounterValue" id="radioCounterValue" value="<?php echo (isset($i))?$i:0; ?>"><!--
<input type="hidden" name="imgOrder" id="imgOrder" value=""> -->
<input type="hidden" name="addInGallery" value="1" />

<div class="row">
	<div class="col-md-12">
		<div class="input_fields_wrap sortable">
			&nbsp;
		</div>
	</div>	
</div>

<div class="row">
	<div class="col-md-6 text-left">
		<a href="#" class="btn btn-primary glyphicon glyphicon-plus add_field_button"><span>Add Images</span></a>
	</div>
	<div class="col-md-6 text-right">
		<button class="btn btn-success glyphicon glyphicon-floppy-disk"><span>Save Gallery</span></button>
	</div>
</div>

</form>
<!-- 
<script type="text/javascript">
	$(document).ready(function(){
		$("a.glyphicon-remove").on("click",function(e){
			e.preventDefault();
			var togo = $(this).attr("href");
			bootbox.confirm({
			    message: "Are you sure you want to delete this teacher?",
			    buttons: {
			        confirm: {
			            label: 'Yes',
			            className: 'btn-success'
			        },
			        cancel: {
			            label: 'No',
			            className: 'btn-danger'
			        }
			    },
			    callback: function (result) {
			        if(result)
			        {
						window.location.href = togo;
			        }
			    }
			});
		})
	});
</script> -->

<script type="text/javascript">
$(document).ready(function() {

	$(".sortable").sortable({
		opacity: 0.9,
			zIndex: 9999,
			containment: "body"
	});

	// var radioCounterValue = document.getElementById('radioCounterValue').value;
	var radioCounterValue = 0;

	var x = radioCounterValue;
    var wrapper         = (".input_fields_wrap"); //Fields wrapper
    var add_button      = (".add_field_button"); //Add button ID
    
    $(add_button).click(function(e){ //on add input button click
	    e.preventDefault();
	    var content_to_add = "\
	    <div class='well ui-state-default' id='well_"+x+"'> \
	    	<div class='row'> \
	    		<div class='col-md-12 text-right'> \
					<a href='#' class='btn btn-xs btn-danger remove_field'>&#10005;</a> \
	    		</div> \
	    	</div> \
		    <div class='row'> \
				<div class='col-md-3'> \
					<label class='form-control btn btn-info btn-file'> \
						<input type='file' name='location_file["+x+"]' placeholder='Upload Picture' class='form-control' onchange='fchanges(this);' required><span class='small'>Upload Picture</span> \
					</label> \
				</div> \
				<div class='col-md-6'> \
					<input type='text' name='img_name["+x+"]' placeholder='Image Name' class='form-control' required \> \
				</div> \
				<div class='col-md-3'> \
					<select name='status["+x+"]' class='form-control' required \> \
						<option value='' selected disabled>Status</option> \
						<option value='active'>Active</option> \
						<option value='inactive'>Inactive</option> \
					</select> \
				</div> \
			</div> \
		</div>";
        x++; //text box increment
        
        $(wrapper).append(content_to_add); //add input box
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); 
        $(this).parent('div').parent('div').parent('div').remove();
    });

    /*setInterval(function(){
		var ids = $(".sortable").sortable("toArray");
		$("#imgOrder").val(ids);
    	console.log($("#imgOrder").val());
    }, 2000);*/
    /*

	$(document).on('change', ':file', function() {
		if($(".btn-file input").val()=="")
		{
			$(".btn-file span").html("Upload Picture");
			$(".btn-file").addClass("btn-info");
			$(".btn-file").removeClass("btn-success");
	        $(".btn-file").siblings("img").remove();
		}
		else
		{
			var filePath = $(".btn-file input").val();
			var filePathArr = filePath.split("\\");
			var filePath = filePathArr[filePathArr.length-1];
			$(".btn-file span").html(filePath);
			$(".btn-file").addClass("btn-success");
			$(".btn-file").removeClass("btn-info");
	        $(".btn-file").siblings("img").remove();
	    	previewImg(this);
		}
	});*/

	$("a.glyphicon-trash").on("click",function(e){
		e.preventDefault();
		var togo = $(this).attr("href");
		bootbox.confirm({
		    message: "Are you sure you want to remove this image from the gallery?",
		    buttons: {
		        confirm: {
		            label: 'Yes',
		            className: 'btn-success'
		        },
		        cancel: {
		            label: 'No',
		            className: 'btn-danger'
		        }
		    },
		    callback: function (result) {
		        if(result)
		        {
					window.location.href = togo;
		        }
		    }
		});
	});
	var radioCounterValue = document.getElementById('radioCounterValue').value;
	localStorage.setItem("count",radioCounterValue);

});

function previewImg(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
        	var c = localStorage.getItem("count");
        	localStorage.setItem("count",++c);
        	imgfile = "<a href='#' data-toggle='modal' data-target='#proimage_"+ c +"'><img src='"+ e.target.result +"' class='img-responsive img-rounded gutter-below' /></a> \
        	<div class='modal fade' id='proimage_"+ c +"' role='dialog'> \
        		<div class='modal-dialog'> \
        			<img src='"+ e.target.result +"' class='img-responsive' /> \
        		</div> \
        	</div> \
        	";
        	$(input).closest(".col-md-3").prepend(imgfile);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function fchanges(temp) {
	var input = $(temp);
	if(input.val()=="")
	{
        input.siblings("span").html("Upload Picture");
		input.closest(".btn-file").addClass("btn-info");
		input.closest(".btn-file").removeClass("btn-success");
        input.closest(".col-md-3").find("img").remove();
	}
	else
	{
    	var filePath = input.val();
		var filePathArr = filePath.split("\\");
		var filePath = filePathArr[filePathArr.length-1];
		input.siblings("span").html(filePath);
		input.closest(".btn-file").addClass("btn-success");
		input.closest(".btn-file").removeClass("btn-info");
		input.closest(".col-md-3").find("img").remove();
    	previewImg(temp);
	}
}
</script>

