<form action="examController.php" method="POST" class="form" enctype="multipart/form-data">
	<div class="row">
		<div class="col-md-2">
			<input type="text" name="ecode" placeholder="Code" class="form-control" required>
		</div>
		<div class="col-md-5">
			<input type="text" name="ename" placeholder="Name" class="form-control" required>
		</div>
		<div class="col-md-5">
			<input type="text" name="etype" placeholder="Type" class="form-control" required>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="panel panel-info">
				<div class="panel-heading text-center">Date</div>
				<div class="panel-body text-center">
					<input type="date" name="edate" min="2017-01-01" max="2020-12-31" value="2017-12-30" class="form-control">
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel panel-info">
				<div class="panel-heading text-center">Time</div>
				<div class="panel-body text-center">
					<input type="time" name="etime" value="00:00" class="form-control">
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel panel-info">
				<div class="panel-heading text-center">Duration</div>
				<div class="panel-body text-center">
					<div class="row">
						<div class="col-md-4"><input type="number" name="edurationh" min="0" max="12" placeholder="H" class="form-control"></div>
						<div class="col-md-4"><input type="number" name="edurationm" min="0" max="59" placeholder="M" class="form-control"></div>
						<div class="col-md-4"><input type="number" name="edurations" min="0" max="59" placeholder="S" class="form-control"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php include("scripts/scr_addquestions.php"); ?>
	<div class="input_fields_wrap sortable">
		<?php
		function read_docx($filename){
			$striped_content = '';
			$content = '';
			if(!$filename || !file_exists($filename)) return false;
			$zip = zip_open($filename);
			if (!$zip || is_numeric($zip)) return false;
			while ($zip_entry = zip_read($zip)) {
					if (zip_entry_open($zip, $zip_entry) == FALSE) continue;
					if (zip_entry_name($zip_entry) != "word/document.xml") continue;
					$content .= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
					zip_entry_close($zip_entry);
			}
			zip_close($zip);
			$content = str_replace('</w:r></w:p></w:tc><w:tc>', " ", $content);
			$content = str_replace('</w:r></w:p>', "\r\n", $content);
			$striped_content = strip_tags($content);
			return $striped_content;
		}
		if(isset($_SESSION['doc_link']))
		{
			$filename = $_SESSION['doc_link'];
			unset($_SESSION['doc_link']);
			$text = read_docx($filename);
			//echo $text;
			$keywords = preg_split('/^\d+\.|^[a-d]+\./m', $text);
			$i=1;
			$j=1;
			foreach ($keywords as $key => $value) {
				if($value!=='
		')
				{
					if($key%5==1)
					{
						$question[$i] = $value;
						$i++;
						$j=1;
					}
					else
					{
						$answer[$i-1][$j] = $value;
						$j++;
					}
				}
			}
			$rc = 0;
			foreach ($question as $q => $que) {
				$a = 0;
		?>
		<div class='well ui-state-default' id='well_<?php echo $rc; ?>'>
			<div class='row'>
				<div class='col-md-7'>
					<input type='text' name='questions[<?php echo $rc; ?>]' placeholder='Type your question here' class='form-control' value="<?php echo $que; ?>" required />
				</div>
				<div class='col-md-4'>
					<input type='text' name='category[<?php echo $rc; ?>]' placeholder='Category' class='form-control' required />
				</div>
				<div class='col-md-1'>
					<a href='#' class='btn btn-xs btn-danger remove_field'>&#10005;</a>
				</div>
			</div>
			<div class='row'>
				<div class='col-md-11'><small class='text-muted small'>Please check the radio button corresponding to the right answer.</small></div>
				<div class='col-md-1'><span class='text-success'>&#10004;</span></div>
			</div>
			<div class='row'>
				<div class='col-md-11'>
					<input type='text' name='choice_a[<?php echo $rc; ?>]' placeholder='Choice A' value="<?php $a++; echo $answer[$q][$a]; ?>" class='form-control' required />
				</div>
				<div class='col-md-1 text-center'>
					<div class='radio'><input type='radio' name='correct[<?php echo $rc; ?>]' value='a' required /></div>
				</div>
			</div>
			<div class='row'>
				<div class='col-md-11'>
					<input type='text' name='choice_b[<?php echo $rc; ?>]' placeholder='Choice B' value="<?php $a++; echo $answer[$q][$a]; ?>" class='form-control' required />
				</div>
				<div class='col-md-1 text-center'>
					<div class='radio'><input type='radio' name='correct[<?php echo $rc; ?>]' value='b' /></div>
				</div>
			</div>
			<div class='row'>
				<div class='col-md-11'>
					<input type='text' name='choice_c[<?php echo $rc; ?>]' placeholder='Choice C' value="<?php $a++; echo $answer[$q][$a]; ?>" class='form-control' required />
				</div>
				<div class='col-md-1 text-center'>
					<div class='radio'><input type='radio' name='correct[<?php echo $rc; ?>]' value='c' /></div>
				</div>
			</div>
			<div class='row'>
				<div class='col-md-11'>
					<input type='text' name='choice_d[<?php echo $rc; ?>]' placeholder='Choice D' value="<?php $a++; echo $answer[$q][$a]; ?>" class='form-control' required />
				</div>
				<div class='col-md-1 text-center'>
					<div class='radio'><input type='radio' name='correct[<?php echo $rc; ?>]' value='d' /></div>
				</div>
			</div>
		</div>
		<?php
				$rc++;
			}
		}
		?>
	</div>
	<input type="hidden" name="radioCounterValue" id="radioCounterValue" value="<?php echo (isset($rc))?$rc:0; ?>">
	<input type="hidden" name="questionsOrder" id="questionsOrder" value="">
	<div class="row">
		<div class="col-md-2">
			<button class="btn btn-primary add_field_button">Add Questions</button>
		</div>
		<div class="col-md-2 col-md-offset-8 text-right">
			<input type="hidden" name="addExam" value="1" />
			<button class="btn btn-lg btn-success">Add Exam</button>
		</div>
	</div>
</form>
<form action="examController.php" method="POST" class="form" enctype="multipart/form-data" id="questionAddFromDoc">
	<input type="hidden" name="addQuestionsFromDoc" />
	<label class="btn btn-primary btn-file">
		Add Questions from File <input type="file" name="add_doc" style="display: none;" />
	</label>
</form>
<script type="text/javascript">
	var radioCounterValue = document.getElementById('radioCounterValue').value;
	$(document).on('change', ':file', function() {
		$("#questionAddFromDoc").submit();
	});
	$(document).ready(function(){
		$(".sortable").sortable({
			opacity: 0.9,
			zIndex: 9999,
			containment: "body"
		});
	// $(".sortable").disableSelection();
		/*$( ".sortable" ).on("sortcreate sortupdate", function( event, ui ) {
			var ids = $(".sortable").sortable("toArray");
			$("#questionsOrder").val(ids);
			console.log(ids);
		});
		$(document).on("click", ".add_field_button, .remove_field", function(){
			var ids = $(".sortable").sortable("toArray");
			$("#questionsOrder").val(ids);
			console.log(ids);
		});*/
		$("form").on("submit",function(){
			var ids = $(".sortable").sortable("toArray");
			$("#questionsOrder").val(ids);
		})
	});
</script>