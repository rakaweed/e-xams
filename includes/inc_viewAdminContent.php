<?php
$aid = $_REQUEST['aid'];
$sqla = "SELECT * FROM tbl_users_admins WHERE aid=".$aid;
$runa = mysqli_query($dbc,$sqla);
$rowa = mysqli_fetch_array($runa);

?>
<div class="row">
	<div class="col-md-10">
	<?php

	echo "<h2 class='role-name'>Admin</h2>
		  <h1>".$rowa['fname']." ".$rowa['lname']."</h1>
		  <h3>".$rowa['gender']."</h3>
		  <h3>".$rowa['email']."</h3>
		  <h3>".$rowa['address']."</h3>
		  <h3>".$rowa['phone']."</h3>
		  <h3>Faculty: ".$rowa['faculty']."</h3>
		  <h3>Post: ".$rowa['post']."</h3>";

	?>
	</div>
	<?php
		$base_dir = "http://localhost/e-xams/";
		$img = ($rowa['propic']=="")?($base_dir."images/exams_.png"):($rowa['propic']);
	?>
	<div class="col-md-2 img-modal">
		<a href="#" data-toggle="modal" data-target="#proimage"><img src="<?php echo $img; ?>" class="img-responsive" id="pro-pic" /></a>
		<div class="modal fade" id="proimage" role="dialog">
			<div class="modal-dialog">
				<img src="<?php echo $img; ?>" class="img-responsive" />
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-1 col-md-offset-10"><a href='<?php echo "editAdmins.php?aid=".$rowa['aid']."'"; ?>' class='btn btn-sm btn-info glyphicon glyphicon-pencil'></a></div>
	<div class="col-md-1"><a href='<?php echo "controller.php?deleteThisAdmin=".$rowa['aid']."'"; ?>' class='btn btn-sm btn-danger glyphicon glyphicon-remove'></a></div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$("a.glyphicon-remove").on("click",function(e){
			e.preventDefault();
			var togo = $(this).attr("href");
			bootbox.confirm({
			    message: "Are you sure you want to delete this admin?",
			    buttons: {
			        confirm: {
			            label: 'Yes',
			            className: 'btn-success'
			        },
			        cancel: {
			            label: 'No',
			            className: 'btn-danger'
			        }
			    },
			    callback: function (result) {
			        if(result)
			        {
						window.location.href = togo;
			        }
			    }
			});
		})
	});
</script>