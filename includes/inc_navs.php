<?php

if(isset($_SESSION['pagename']))
{
	echo "<h1 class='text-center page-name'>".$_SESSION['pagename']."</h1><hr />";
}

echo '<a href="userIndex.php" class="btn btn-block btn-info btn-lg">Profile</a>';

if($_SESSION['role']=="admin")
{
?>

<a href="admins.php" class="btn btn-block btn-info btn-lg">Admins</a>
<a href="teachers.php" class="btn btn-block btn-info btn-lg">Teachers</a>
<a href="students.php" class="btn btn-block btn-info btn-lg">Students</a>
<a href="exams.php" class="btn btn-block btn-info btn-lg">Exams</a>
<a href="gallery.php" class="btn btn-block btn-info btn-lg">Gallery</a>

<?php
}

else if($_SESSION['role']=="teacher")
{
?>

<a href="students.php" class="btn btn-block btn-info btn-lg">Students</a>
<a href="exams.php" class="btn btn-block btn-info btn-lg">Exams</a>

<?php
}

else
{
?>
<!-- 
<button class="btn btn-block btn-lg">Request Change</button> -->
<a href="startExam.php" class="btn btn-info btn-block btn-lg">Start Exam</a>
<a href="results.php" class="btn btn-info btn-block btn-lg">Results</a>
<?php
}
?>