<?php

if(isset($_GET['eid']) AND !empty($_GET['eid']))
{
	$sql = "SELECT * FROM tbl_exams WHERE eid=".$_GET['eid'];
	$run = mysqli_query($dbc,$sql);
	$count = mysqli_num_rows($run);
	if($count>0)
	{
		$row = mysqli_fetch_array($run);
		$sec = $row['eduration'];
		$hour = (int)($sec / (60 * 60));
		$sec -= $hour * (60 * 60);
		$min = (int)($sec / 60);
		$sec -= $min * 60;
	?>

<form action="examController.php" method="POST" class="form" enctype="multipart/form-data">
	<div class="row">
		<input type="hidden" name="eid" value="<?php echo $row["eid"]; ?>" />
		<div class="col-md-2">
			<input type="text" name="ecode" placeholder="Code" class="form-control" value="<?php echo $row['ecode']; ?>" required>
		</div>
		<div class="col-md-5">
			<input type="text" name="ename" placeholder="Name" class="form-control" value="<?php echo $row['ename']; ?>" required>
		</div>
		<div class="col-md-5">
			<input type="text" name="etype" placeholder="Type" class="form-control" value="<?php echo $row['etype']; ?>" required>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="panel panel-info">
				<div class="panel-heading text-center">Date</div>
				<div class="panel-body text-center">
					<input type="date" name="edate" min="2017-01-01" max="2020-12-31" value="<?php echo $row['edate']; ?>" class="form-control">
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel panel-info">
				<div class="panel-heading text-center">Time</div>
				<div class="panel-body text-center">
					<input type="time" name="etime" value="<?php echo $row['etime']; ?>" class="form-control">
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel panel-info">
				<div class="panel-heading text-center">Duration</div>
				<div class="panel-body text-center">
					<div class="row">
						<div class="col-md-4"><input type="number" name="edurationh" min="0" max="12" placeholder="H" class="form-control" value="<?php echo $hour; ?>"></div>
						<div class="col-md-4"><input type="number" name="edurationm" min="0" max="59" placeholder="M" class="form-control" value="<?php echo $min; ?>"></div>
						<div class="col-md-4"><input type="number" name="edurations" min="0" max="59" placeholder="S" class="form-control" value="<?php echo $sec; ?>"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php include("scripts/scr_addquestions.php"); ?>
	<div class="input_fields_wrap sortable">
		<?php
		
		$sqlq = "SELECT * FROM tbl_questions WHERE eid=".$row['eid']." ORDER BY category asc, qorder asc";
		$runq = mysqli_query($dbc,$sqlq);
		$countq = mysqli_num_rows($runq);

		if($countq>0)
		{	
			$rc = 0;
			while($rowq = mysqli_fetch_array($runq))
			{
				$a = 0;
		?>
		<div class='well ui-state-default' id='well_<?php echo $rc; ?>'>
			<div class='row'>
				<div class='col-md-7'>
					<input type="hidden" name="qid[<?php echo $rc; ?>]" value="<?php echo $rowq["qid"]; ?>" />
					<input type='text' name='questions[<?php echo $rc; ?>]' placeholder='Type your question here' class='form-control' value='<?php echo $rowq["question"]; ?>' required />
				</div>
				<div class='col-md-4'>
					<input type='text' name='category[<?php echo $rc; ?>]' placeholder='Category' class='form-control' value='<?php echo $rowq["category"]; ?>' required />
				</div>
				<div class='col-md-1'>
					<a href='#' class='btn btn-xs btn-danger remove_field'>&#10005;</a>
				</div>
			</div>
			<div class='row'>
				<div class='col-md-11'><small class='text-muted small'>Please check the radio button corresponding to the right answer.</small></div>
				<div class='col-md-1'><span class='text-success'>&#10004;</span></div>
			</div>
			<div class='row'>
				<div class='col-md-11'>
					<input type='text' name='choice_a[<?php echo $rc; ?>]' placeholder='Choice A' value="<?php $a++; echo $rowq['choice_a']; ?>" class='form-control' required />
				</div>
				<div class='col-md-1 text-center'>
					<div class='radio'><input type='radio' name='correct[<?php echo $rc; ?>]' value='a' <?php echo ($rowq['correct']=="choice_a")?("checked='checked'"):""; ?> required /></div>
				</div>
			</div>
			<div class='row'>
				<div class='col-md-11'>
					<input type='text' name='choice_b[<?php echo $rc; ?>]' placeholder='Choice B' value="<?php $a++; echo $rowq['choice_b']; ?>" class='form-control' required />
				</div>
				<div class='col-md-1 text-center'>
					<div class='radio'><input type='radio' name='correct[<?php echo $rc; ?>]' value='b' <?php echo ($rowq['correct']=="choice_b")?("checked='checked'"):""; ?> /></div>
				</div>
			</div>
			<div class='row'>
				<div class='col-md-11'>
					<input type='text' name='choice_c[<?php echo $rc; ?>]' placeholder='Choice C' value="<?php $a++; echo $rowq['choice_c']; ?>" class='form-control' required />
				</div>
				<div class='col-md-1 text-center'>
					<div class='radio'><input type='radio' name='correct[<?php echo $rc; ?>]' value='c' <?php echo ($rowq['correct']=="choice_c")?("checked='checked'"):""; ?> /></div>
				</div>
			</div>
			<div class='row'>
				<div class='col-md-11'>
					<input type='text' name='choice_d[<?php echo $rc; ?>]' placeholder='Choice D' value="<?php $a++; echo $rowq['choice_d']; ?>" class='form-control' required />
				</div>
				<div class='col-md-1 text-center'>
					<div class='radio'><input type='radio' name='correct[<?php echo $rc; ?>]' value='d' <?php echo ($rowq['correct']=="choice_d")?("checked='checked'"):""; ?> /></div>
				</div>
			</div>
		</div>
		<?php
				$rc++;
			}
		}
		?>
	</div>
	<input type="hidden" name="radioCounterValue" id="radioCounterValue" value="<?php echo (isset($rc))?$rc:0; ?>">
	<input type="hidden" name="questionsOrder" id="questionsOrder" value="">
	<div class="row">
		<div class="col-md-2">
			<button class="btn btn-primary add_field_button">Add Questions</button>
		</div>
		<div class="col-md-2 col-md-offset-8 text-right">
			<input type="hidden" name="editExam" value="1" />
			<button class="btn btn-lg btn-success">Update Exam</button>
		</div>
	</div>
</form>

<script type="text/javascript">
	var radioCounterValue = document.getElementById('radioCounterValue').value;

	$(document).ready(function(){
		$(".sortable").sortable({
			opacity: 0.9,
 			zIndex: 9999,
 			containment: "body"
		});

    	// $(".sortable").disableSelection();

		/*$( ".sortable" ).on("sortcreate sortupdate", function( event, ui ) {
			var ids = $(".sortable").sortable("toArray");
			$("#questionsOrder").val(ids);
			console.log(ids);
		});

		$(document).on("click", ".add_field_button, .remove_field", function(){
			var ids = $(".sortable").sortable("toArray");
			$("#questionsOrder").val(ids);
			console.log(ids);
		});*/

		$("form").on("submit",function(){
			var ids = $(".sortable").sortable("toArray");
			$("#questionsOrder").val(ids);
		})
	});
</script>

	<?php
	}
	else
	{
		$_SESSION['error'] = "The specified exam does not exist.";
		header("Location: exams.php");
	}
}
else
{
		$_SESSION['error'] = "Exam ID must be specified.";
		header("Location: exams.php");
}

?>