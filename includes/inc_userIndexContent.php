	<div class="row">
		<div class="col-md-10">
			
			<?php

			if($_SESSION['role']=="admin")
			{
				echo "<h2 class='role-name'>Admin</h1>
					  <h1>".$row['fname']." ".$row['lname']."</h1>
					  <h3>".$row['gender']."</h4>
					  <h3>".$row['email']."</h3>
					  <h3>".$row['address']."</h3>
					  <h3>".$row['phone']."</h3>
					  <h3>Faculty: ".$row['faculty']."</h3>
					  <h3>Post: ".$row['post']."</h3>";
			}

			else if($_SESSION['role']=="teacher")
			{
				echo "<h2 class='role-name'>Teacher</h1>
					  <h1>".$row['fname']." ".$row['lname']."</h1>
					  <h3>".$row['gender']."</h4>
					  <h3>".$row['email']."</h3>
					  <h3>".$row['address']."</h3>
					  <h3>".$row['phone']."</h3>
					  <h3>Faculty: ".$row['faculty']."</h3>
					  <h3>Post: ".$row['post']."</h3>";
			}

			else
			{
				echo "<h2 class='role-name'>Student</h1>
					  <h1>".$row['fname']." ".$row['lname']."</h1>
					  <h3>".$row['gender']."</h4>
					  <h3>".$row['email']."</h3>
					  <h3>".$row['address']."</h3>
					  <h3>".$row['phone']."</h3>
					  <h3>Graduated Level: ".$row['grad_level']."</h3>
					  <h3>Graduated Year: ".$row['grad_year']."</h3>
					  <h3>Applying For: ";
				echo ($row['apply_for']!="")?($row['apply_for']):($row['attended_exam']." (Exam Attended)");
				echo "</h3>
					  <h3>Score: ".$row['score']."</h3>";
			}
			?>

		</div>
		<?php
			$base_dir = "http://localhost/e-xams/";
			$img = ($row['propic']=="")?($base_dir."images/exams_.png"):($row['propic']);
		?>
		<div class="col-md-2 img-modal">
		<a href="#" data-toggle="modal" data-target="#proimage"><img src="<?php echo $img; ?>" class="img-responsive" id="pro-pic" /></a>
		<div class="modal fade" id="proimage" role="dialog">
			<div class="modal-dialog">
				<img src="<?php echo $img; ?>" class="img-responsive" />
			</div>
		</div>
	</div>
	</div>
