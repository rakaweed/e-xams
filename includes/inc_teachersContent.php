<?php
if(isset($_GET['viewall']))
	$sqlt = "SELECT t.*, u.username, u.status FROM tbl_users_admins AS t JOIN tbl_users AS u ON t.uid=u.uid WHERE u.role='teacher' ORDER BY t.aid desc";
else
	$sqlt = "SELECT t.*, u.username, u.status FROM tbl_users_admins AS t JOIN tbl_users AS u ON t.uid=u.uid WHERE u.role='teacher' AND u.status='active' ORDER BY t.aid desc";

$runt = mysqli_query($dbc,$sqlt);
$countt = mysqli_num_rows($runt);
$t = 1;

?>
<div class="row">
	<div class="col-md-12 text-right">
		<?php 
			echo isset($_GET['viewall'])?'<a href="?viewactive" class="btn btn-sm btn-default glyphicon glyphicon-eye-close" title="View Active Only"></a>':'<a href="?viewall" class="btn btn-sm btn-default glyphicon glyphicon-eye-open" title="View All"></a>'; 
		?>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<?php
		if($countt>0)
		{
		?>
		<table class="table table-hover table-bordered table-condensed">
			<thead>
				<tr>
					<th>S. No.</th>
					<th>Name</th>
					<th>Username</th>
					<th>Gender</th>
					<th>Faculty</th>
					<th>Post</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			<?php
			while ($rowt = mysqli_fetch_array($runt))
			{
				echo "
				<tr ";
				echo ($rowt['status']=='inactive')?("class='danger'"):"";
				echo ">
					<td>$t</td>
					<td><a href='viewTeacher.php?tid=".$rowt['aid']."' title='View Teacher'>".$rowt['fname']." ".$rowt['lname']."</a></td>
					<td>".$rowt['username']."</td>
					<td>".$rowt['gender']."</td>
					<td>".$rowt['faculty']."</td>
					<td>".$rowt['post']."</td>
					<td>				
					<a href='editTeachers.php?tid=".$rowt['aid']."' class='btn btn-sm btn-info glyphicon glyphicon-pencil'><span>Edit</span></a>
					<a href='controller.php?deleteThisTeacher=".$rowt['aid']."' class='btn btn-sm btn-danger glyphicon glyphicon-remove'><span>Delete</span></a>
					</td>
				</tr>
				";
				$t++;
			}

			?>	
			</tbody>
		</table>
		<?php
		}
		?>		
		<div class="text-right">
			<a href="addTeachers.php" class="btn btn-success glyphicon glyphicon-plus"><span>Add</span></a>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$("a.glyphicon-remove").on("click",function(e){
			e.preventDefault();
			var togo = $(this).attr("href");
			bootbox.confirm({
			    message: "Are you sure you want to delete this teacher?",
			    buttons: {
			        confirm: {
			            label: 'Yes',
			            className: 'btn-success'
			        },
			        cancel: {
			            label: 'No',
			            className: 'btn-danger'
			        }
			    },
			    callback: function (result) {
			        if(result)
			        {
						window.location.href = togo;
			        }
			    }
			});
		})
	});
</script>