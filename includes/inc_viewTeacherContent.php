<?php
$tid = $_REQUEST['tid'];
$sqlt = "SELECT * FROM tbl_users_admins WHERE aid=".$tid;
$runt = mysqli_query($dbc,$sqlt);
$rowt = mysqli_fetch_array($runt);

?>
<div class="row">
	<div class="col-md-10">
	<?php

	echo "<h2 class='role-name'>Teacher</h2>
		  <h1>".$rowt['fname']." ".$rowt['lname']."</h1>
		  <h3>".$rowt['gender']."</h3>
		  <h3>".$rowt['email']."</h3>
		  <h3>".$rowt['address']."</h3>
		  <h3>".$rowt['phone']."</h3>
		  <h3>Faculty: ".$rowt['faculty']."</h3>
		  <h3>Post: ".$rowt['post']."</h3>";

	?>
	</div>
	<?php
		$base_dir = "http://localhost/e-xams/";
		$img = ($rowt['propic']=="")?($base_dir."images/exams_.png"):($rowt['propic']);
	?>
	<div class="col-md-2 img-modal">
		<a href="#" data-toggle="modal" data-target="#proimage"><img src="<?php echo $img; ?>" class="img-responsive" id="pro-pic" /></a>
		<div class="modal fade" id="proimage" role="dialog">
			<div class="modal-dialog">
				<img src="<?php echo $img; ?>" class="img-responsive" />
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-1 col-md-offset-10"><a href='<?php echo "editTeachers.php?tid=".$rowt['aid']."'"; ?>' class='btn btn-sm btn-info glyphicon glyphicon-pencil'></a></div>
	<div class="col-md-1"><a href='<?php echo "controller.php?deleteThisTeacher=".$rowt['aid']."'"; ?>' class='btn btn-sm btn-danger glyphicon glyphicon-remove'></a></div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$("a.glyphicon-remove").on("click",function(e){
			e.preventDefault();
			var togo = $(this).attr("href");
			bootbox.confirm({
			    message: "Are you sure you want to delete this teacher?",
			    buttons: {
			        confirm: {
			            label: 'Yes',
			            className: 'btn-success'
			        },
			        cancel: {
			            label: 'No',
			            className: 'btn-danger'
			        }
			    },
			    callback: function (result) {
			        if(result)
			        {
						window.location.href = togo;
			        }
			    }
			});
		})
	});
</script>