<?php

if(isset($_GET['viewall']))
	$sqls = "SELECT s.*, u.username, u.status FROM tbl_users_students AS s JOIN tbl_users AS u ON s.uid=u.uid ORDER BY s.sid desc";
else
	$sqls = "SELECT s.*, u.username, u.status FROM tbl_users_students AS s JOIN tbl_users AS u ON s.uid=u.uid WHERE status='active' ORDER BY s.sid desc";

$runs = mysqli_query($dbc,$sqls);
$counts = mysqli_num_rows($runs);
$s = 1;

?>
<div class="row">
	<div class="col-md-12 text-right">
		<?php 
			echo isset($_GET['viewall'])?'<a href="?viewactive" class="btn btn-sm btn-default glyphicon glyphicon-eye-close" title="View Active Only"></a>':'<a href="?viewall" class="btn btn-sm btn-default glyphicon glyphicon-eye-open" title="View All"></a>'; 
		?>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<?php
		if($counts>0)
		{
		?>
		<table class="table table-hover table-bordered table-condensed">
			<thead>
				<tr>
					<th>S. No.</th>
					<th>Name</th>
					<th>Username</th>
					<th>Email</th>
					<th>Gender</th>
					<th>Assigned</th>
					<th>Score</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			<?php
			while ($rows = mysqli_fetch_array($runs))
			{
				echo "
				<tr ";
				echo ($rows['status']=='inactive')?("class='danger'"):"";
				echo ">
					<td>$s</td>
					<td><a href='viewStudent.php?sid=".$rows['sid']."' title='View Student'>".$rows['fname']." ".$rows['lname']."</a></td>
					<td>".$rows['username']."</td>
					<td>".$rows['email']."</td>
					<td>".$rows['gender']."</td>
					<td>";
					
				echo ($rows['apply_for']=='' AND $rows['attended_exam']=='')?("No"):(($rows['attended_exam']!='')?("Exam Attended (".$rows['attended_exam'].")"):("Yes (".$rows['apply_for'].")"));
					
				echo "</td>
					<td><a href='results.php?sid=".$rows['sid']."' title='View Student Results'>".$rows['score']."</a></td>
					<td>
					<a href='editStudents.php?sid=".$rows['sid']."' class='btn btn-sm btn-info glyphicon glyphicon-pencil'><span>Edit</span></a>
					<a href='controller.php?deleteThisStudent=".$rows['sid']."' class='btn btn-sm btn-danger glyphicon glyphicon-remove'><span>Delete</span></a>
					</td>
				</tr>
				";
				$s++;
			}

			?>	
			</tbody>
		</table>
		<?php
		}
		?>		
		<div class="text-right">
			<a href="addStudents.php" class="btn btn-success glyphicon glyphicon-plus"><span>Add</span></a>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$("a.glyphicon-remove").on("click",function(e){
			e.preventDefault();
			var togo = $(this).attr("href");
			bootbox.confirm({
			    message: "Are you sure you want to delete this student?",
			    buttons: {
			        confirm: {
			            label: 'Yes',
			            className: 'btn-success'
			        },
			        cancel: {
			            label: 'No',
			            className: 'btn-danger'
			        }
			    },
			    callback: function (result) {
// window.location.href = "http://stackoverflow.com";
			        if(result)
			        {
						window.location.href = togo;
			        }
			    }
			});
		})
	});
</script>