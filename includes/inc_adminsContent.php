<?php
if(isset($_GET['viewall']))
	$sqla = "SELECT a.*, u.username, u.status FROM tbl_users_admins AS a JOIN tbl_users AS u ON a.uid=u.uid WHERE u.role='admin' ORDER BY a.aid desc";
else
	$sqla = "SELECT a.*, u.username, u.status FROM tbl_users_admins AS a JOIN tbl_users AS u ON a.uid=u.uid WHERE u.role='admin' AND u.status='active' ORDER BY a.aid desc";

$runa = mysqli_query($dbc,$sqla);
$counta = mysqli_num_rows($runa);
$a = 1;

?>
<div class="row">
	<div class="col-md-12 text-right">
		<?php 
			echo isset($_GET['viewall'])?'<a href="?viewactive" class="btn btn-sm btn-default glyphicon glyphicon-eye-close" title="View Active Only"></a>':'<a href="?viewall" class="btn btn-sm btn-default glyphicon glyphicon-eye-open" title="View All"></a>'; 
		?>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<?php
		if($counta>0)
		{
		?>
		<table class="table table-hover table-bordered table-condensed">
			<thead>
				<tr>
					<th>S. No.</th>
					<th>Name</th>
					<th>Username</th>
					<th>Gender</th>
					<th>Faculty</th>
					<th>Post</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			<?php
			while ($rowa = mysqli_fetch_array($runa))
			{
				echo "
				<tr ";
				echo ($rowa['status']=='inactive')?("class='danger'"):"";
				echo ">
					<td>$a</td>
					<td><a href='viewAdmin.php?aid=".$rowa['aid']."' title='View Admin'>".$rowa['fname']." ".$rowa['lname']."</a></td>
					<td>".$rowa['username']."</td>
					<td>".$rowa['gender']."</td>
					<td>".$rowa['faculty']."</td>
					<td>".$rowa['post']."</td>
					<td>				
					<a href='editAdmins.php?aid=".$rowa['aid']."' class='btn btn-sm btn-info glyphicon glyphicon-pencil'><span>Edit</span></a>
					<a href='controller.php?deleteThisAdmin=".$rowa['aid']."' class='btn btn-sm btn-danger glyphicon glyphicon-remove'><span>Delete</span></a>
					</td>
				</tr>
				";
				$a++;
			}

			?>	
			</tbody>
		</table>
		<?php
		}
		?>		
		<div class="text-right">
			<a href="addAdmins.php" class="btn btn-success glyphicon glyphicon-plus"><span>Add</span></a>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$("a.glyphicon-remove").on("click",function(e){
			e.preventDefault();
			var togo = $(this).attr("href");
			bootbox.confirm({
			    message: "Are you sure you want to delete this admin?",
			    buttons: {
			        confirm: {
			            label: 'Yes',
			            className: 'btn-success'
			        },
			        cancel: {
			            label: 'No',
			            className: 'btn-danger'
			        }
			    },
			    callback: function (result) {
			        if(result)
			        {
						window.location.href = togo;
			        }
			    }
			});
		})
	});
</script>