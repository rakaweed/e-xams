-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 13, 2018 at 05:41 AM
-- Server version: 10.1.24-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_exams`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_exams`
--

CREATE TABLE `tbl_exams` (
  `eid` int(11) NOT NULL,
  `ecode` varchar(10) NOT NULL,
  `ename` varchar(100) NOT NULL,
  `etype` varchar(50) NOT NULL,
  `edate` date NOT NULL,
  `etime` time NOT NULL,
  `eduration` int(11) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gallery`
--

CREATE TABLE `tbl_gallery` (
  `imgid` int(11) NOT NULL,
  `img_name` varchar(250) NOT NULL,
  `location` varchar(250) NOT NULL,
  `appear_order` int(11) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `qid` int(11) NOT NULL,
  `eid` int(11) NOT NULL,
  `question` varchar(100) NOT NULL,
  `category` varchar(50) NOT NULL,
  `choice_a` varchar(100) NOT NULL,
  `choice_b` varchar(100) NOT NULL,
  `choice_c` varchar(100) NOT NULL,
  `choice_d` varchar(100) NOT NULL,
  `correct` varchar(10) NOT NULL,
  `qorder` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_results`
--

CREATE TABLE `tbl_results` (
  `rid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `eid` int(11) NOT NULL,
  `total_questions` int(11) NOT NULL,
  `correct` int(11) NOT NULL,
  `wrong` int(11) NOT NULL,
  `unanswered` int(11) NOT NULL,
  `timetaken` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `uid` int(11) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL,
  `role` varchar(10) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`uid`, `username`, `password`, `role`, `status`) VALUES
(1, 'ad', 'min', 'admin', 'active'),
(2, 'tea', 'cher  ', 'teacher', 'active'),
(3, 'stu', 'dent', 'student', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users_admins`
--

CREATE TABLE `tbl_users_admins` (
  `aid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `fname` varchar(37) NOT NULL,
  `lname` varchar(37) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `address` varchar(100) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `email` varchar(44) NOT NULL,
  `faculty` varchar(44) NOT NULL,
  `post` varchar(20) NOT NULL,
  `propic` longblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users_admins`
--

INSERT INTO `tbl_users_admins` (`aid`, `uid`, `fname`, `lname`, `gender`, `address`, `phone`, `email`, `faculty`, `post`, `propic`) VALUES
(1, 1, 'Admin', 'Person', 'Male', 'Somewhere', '0000000000', 'admin@email.com', 'CSIT', 'Administrator', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users_students`
--

CREATE TABLE `tbl_users_students` (
  `sid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `fname` varchar(37) NOT NULL,
  `lname` varchar(37) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `address` varchar(100) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `email` varchar(44) NOT NULL,
  `birthday` date NOT NULL,
  `propic` longblob NOT NULL,
  `grad_level` varchar(44) NOT NULL,
  `grad_year` varchar(10) NOT NULL,
  `apply_for` varchar(44) NOT NULL,
  `score` int(11) NOT NULL,
  `attended_exam` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_exams`
--
ALTER TABLE `tbl_exams`
  ADD PRIMARY KEY (`eid`);

--
-- Indexes for table `tbl_gallery`
--
ALTER TABLE `tbl_gallery`
  ADD PRIMARY KEY (`imgid`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`qid`);

--
-- Indexes for table `tbl_results`
--
ALTER TABLE `tbl_results`
  ADD PRIMARY KEY (`rid`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `tbl_users_admins`
--
ALTER TABLE `tbl_users_admins`
  ADD PRIMARY KEY (`aid`);

--
-- Indexes for table `tbl_users_students`
--
ALTER TABLE `tbl_users_students`
  ADD PRIMARY KEY (`sid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_exams`
--
ALTER TABLE `tbl_exams`
  MODIFY `eid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_gallery`
--
ALTER TABLE `tbl_gallery`
  MODIFY `imgid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `qid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_results`
--
ALTER TABLE `tbl_results`
  MODIFY `rid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tbl_users_admins`
--
ALTER TABLE `tbl_users_admins`
  MODIFY `aid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_users_students`
--
ALTER TABLE `tbl_users_students`
  MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
