<?php

if(isset($_GET['viewall']))
	$sqle = "SELECT * FROM tbl_exams ORDER BY eid";
else
	$sqle = "SELECT * FROM tbl_exams WHERE status='active' ORDER BY eid";
$rune = mysqli_query($dbc,$sqle);
$counte = mysqli_num_rows($rune);
$e = 1;

?>
<div class="row">
	<div class="col-md-12 text-right">
		<?php 
			echo isset($_GET['viewall'])?'<a href="?viewactive" class="btn btn-sm btn-default glyphicon glyphicon-eye-close" title="View Active Only"></a>':'<a href="?viewall" class="btn btn-sm btn-default glyphicon glyphicon-eye-open" title="View All"></a>'; 
		?>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<?php
		if($counte>0)
		{
		?>
		<table class="table table-hover table-bordered table-condensed">
			<thead>
				<tr>
					<th>S. No.</th>
					<th>Code</th>
					<th>Name</th>
					<th>Type</th>
					<th>Date</th>
					<th>Time</th>
					<th>Duration</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			<?php
			while ($rowe = mysqli_fetch_array($rune))
			{
				$sec = $rowe['eduration'];
				$hour = (int)($sec / (60 * 60));
				$sec -= $hour * (60 * 60);
				$min = (int)($sec / 60);
				$sec -= $min * 60;
				echo "
				<tr ";
				echo ($rowe['status']=='inactive')?("class='danger'"):"";
				echo ">
					<td>$e</td>
					<td>".$rowe['ecode']."</td>
					<td>".$rowe['ename']."</td>
					<td>".$rowe['etype']."</td>
					<td>".$rowe['edate']."</td>
					<td>".$rowe['etime']."</td>
					<td>".$hour."h ".$min."m ".$sec."s</td>
					<td>
					<a href='editExams.php?eid=".$rowe['eid']."' class='btn btn-sm btn-info glyphicon glyphicon-pencil'><span>Edit</span></a>
					<a href='examController.php?deleteThisExam=".$rowe['eid'];
				echo $rowe['status']=="inactive"?"&reallyDeleteThisExam":"";
				echo "' class='btn btn-sm btn-danger glyphicon glyphicon-remove'><span>Delete</span></a>

					</td>
				</tr>
				";
				$e++;
			}

			?>	
			</tbody>
		</table>
		<?php
		}
		?>
		<div class="text-right">
			<a href="addExams.php" class="btn btn-success glyphicon glyphicon-plus"><span>Add</span></a>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$("a.glyphicon-remove").on("click",function(e){
			e.preventDefault();
			var togo = $(this).attr("href");
			bootbox.confirm({
			    message: "Are you sure you want to delete this exam?",
			    buttons: {
			        confirm: {
			            label: 'Yes',
			            className: 'btn-success'
			        },
			        cancel: {
			            label: 'No',
			            className: 'btn-danger'
			        }
			    },
			    callback: function (result) {
			        if(result)
			        {
						window.location.href = togo;
			        }
			    }
			});
		})
	});
</script>