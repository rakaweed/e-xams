<?php session_start(); 
include("includes/inc_dbc.php");

$_SESSION['pagename'] = "E-Xams";
if(isset($_SESSION['user']))
{
	header("Location: userIndex.php");
}
else
{
	$sqlg = "SELECT * FROM tbl_gallery WHERE status='active' ORDER BY appear_order";
	$rung = mysqli_query($dbc,$sqlg);
	$countg = $rung?mysqli_num_rows($rung):0;
?>
<!DOCTYPE html>
<html>
<head>
	<title>E-Xams</title>
	<?php include("includes/inc_styles.php"); ?>
</head>
<body>

<div class="jumbotron">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<?php include('includes/inc_logo.php'); ?>
			</div>
			<div class="col-md-6">
				<form action="controller.php" method="POST" enctype="multipart/form-data" class="form-inline">
					<input type="text" name="uname" placeholder="Username" class="form-control" />
					<input type="password" type="stars" name="passw" placeholder="Password" class="form-control" />
					<button class="btn btn-info glyphicon glyphicon-log-in" name="login-submit"><span>Log In</span></button>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<?php
			if(isset($_SESSION['error']))
			{	
				echo "<div class='col-md-12'>";
				echo "<p class='alert alert-danger' id='msg-err'>".$_SESSION['error']."</p>";
				echo "</div>";
				unset($_SESSION['error']);
			}
			else if (isset($_SESSION['success'])) 
			{
				echo "<div class='col-md-12'>";
				echo "<p class='alert alert-success' id='msg-succ'>".$_SESSION['success']."</p>";
				echo "</div>";
				unset($_SESSION['success']);
			}
		?>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?php
				if($countg>0)
				{
					$c = 0;
					$indicators = "";
					$items = ""; 
					while($rowg = mysqli_fetch_array($rung))
					{
						$indicators .= '
							<li data-target="#myCarousel" data-slide-to="'.$c.'"';
						$indicators .= ($c==0)?'class="active"':'';
						$indicators .= '></li>';
						
						$items .= '
							<div class="item';
						$items .= ($c==0)?" active":"";
						$items .= '">
								<img src="uploads/img_gallery/'.$rowg["location"].'" alt="'.$rowg["img_name"].'" />
							</div>
						';
						$c++;
					}
			?>
					<div id="myCarousel" class="carousel slide" data-ride="carousel">

						<!-- Indicators -->
						<ol class="carousel-indicators">
							<?php echo $indicators; ?>
						</ol>

						<!-- Wrapper for slides -->
						<div class="carousel-inner">
							<?php echo $items; ?>
						</div>

						<!-- Left and right controls -->
						<a class="left carousel-control" href="#myCarousel" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="right carousel-control" href="#myCarousel" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right"></span>
							<span class="sr-only">Next</span>
						</a>
					</div>
			<?php
				}
			?>
		</div>
	</div>
<?php include("includes/inc_footer.php"); ?>
</div>
</body>
</html>
<?php
}
?>