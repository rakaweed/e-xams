<?php 
session_start(); 
include("includes/inc_dbc.php");

$_SESSION['pagename'] = "Examination";
if(isset($_SESSION['user']))
{
	if($_SESSION['role']=='student')
	{
		$user = $_SESSION['user'];

		$sql = "SELECT * FROM tbl_users_students WHERE uid=$user";
		$run = mysqli_query($dbc,$sql);
		$row = mysqli_fetch_array($run);
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo isset($_SESSION['pagename'])?$_SESSION['pagename']." | ":""; ?>E-Xams</title>
	<?php include("includes/inc_styles.php"); ?>
</head>
<body>

<div class="jumbotron">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<?php include('includes/inc_logo.php'); ?>
			</div>
			<div class="col-md-6 text-right">
				<?php include('includes/inc_logout.php'); ?>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-3">
			<div class="well">
				<?php include("includes/inc_navs.php"); ?>
			</div>
		</div>
		<div class="col-md-9">
			<?php include("includes/inc_startExamContent.php");	?>
		</div>	
	</div>
	<?php include("includes/inc_footer.php"); ?>
</div>
</body>
</html>
<?php
	}
	else
	{
		header("Location: exams.php");
	}
}
else
{
	$_SESSION['error'] = "Please login to continue.";
	header("Location: index.php");
}
?>