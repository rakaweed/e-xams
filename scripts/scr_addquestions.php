<script type="text/javascript">
$(document).ready(function() {

	/*function getCookie(cname) {
	    var name = cname + "=";
	    var decodedCookie = decodeURIComponent(document.cookie);
	    var ca = decodedCookie.split(';');
	    for(var i = 0; i <ca.length; i++) {
	        var c = ca[i];
	        while (c.charAt(0) == ' ') {
	            c = c.substring(1);
	        }
	        if (c.indexOf(name) == 0) {
	            return c.substring(name.length, c.length);
	        }
	    }
	    return "";
	}*/
/*
	if(!getCookie('radioCounter')){document.cookie = "radioCounter=" + 0 + ";path=/";}*/

	//alert(getCookie('radioCounter'));
	//alert(document.cookie);
	// var x = getCookie('radioCounter');
	var x = radioCounterValue;
    var wrapper         = (".input_fields_wrap"); //Fields wrapper
    var add_button      = (".add_field_button"); //Add button ID
    
    $(add_button).click(function(e){ //on add input button click
	    e.preventDefault();
	        
	    var content_to_add = "\
	    <div class='well' id='well_"+x+"'> \
		    <div class='row'> \
				<div class='col-md-7'> \
					<input type='text' name='questions["+x+"]' placeholder='Type your question here' class='form-control' required \> \
				</div> \
				<div class='col-md-4'> \
					<input type='text' name='category["+x+"]' placeholder='Category' class='form-control' required \> \
				</div> \
				<div class='col-md-1'> \
					<a href='#' class='btn btn-xs btn-danger remove_field'>&#10005;</a> \
				</div> \
			</div> \
			<div class='row'> \
				<div class='col-md-11'><small class='text-muted small'>Please check the radio button corresponding to the right answer.</small></div> \
				<div class='col-md-1'><span class='text-success'>&#10004;</span></div> \
			</div> \
			<div class='row'> \
				<div class='col-md-11'> \
					<input type='text' name='choice_a["+x+"]' placeholder='Choice A' class='form-control' required \> \
				</div> \
				<div class='col-md-1 text-center'> \
					<div class='radio'><input type='radio' name='correct["+x+"]' value='a' required \></div> \
				</div> \
			</div> \
			<div class='row'> \
				<div class='col-md-11'> \
					<input type='text' name='choice_b["+x+"]' placeholder='Choice B' class='form-control' required \> \
				</div> \
				<div class='col-md-1 text-center'> \
					<div class='radio'><input type='radio' name='correct["+x+"]' value='b' \></div> \
				</div> \
			</div> \
			<div class='row'> \
				<div class='col-md-11'> \
					<input type='text' name='choice_c["+x+"]' placeholder='Choice C' class='form-control' required \> \
				</div> \
				<div class='col-md-1 text-center'> \
					<div class='radio'><input type='radio' name='correct["+x+"]' value='c' \></div> \
				</div> \
			</div> \
			<div class='row'> \
				<div class='col-md-11'> \
					<input type='text' name='choice_d["+x+"]' placeholder='Choice D' class='form-control' required \> \
				</div> \
				<div class='col-md-1 text-center'> \
					<div class='radio'><input type='radio' name='correct["+x+"]' value='d' \></div> \
				</div> \
			</div> \
		</div>";
        x++; //text box increment
        /*var d = new Date();
        document.cookie = "radioCounter=" + x + "; expires=" + (d.getTime())*30*24*60*60 + "; path=/";*/
        $(wrapper).append(content_to_add); //add input box
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); 
        $(this).parent('div').parent('div').parent('div').remove();
    });


    
   //  $(".add_doc_wrapper").on("click",".add_doc", function(e){ //user click on remove text
   //      e.preventDefault();

   //      <?php

			// $filename = "questions.docx";
			// $text = read_docx($filename);

			// echo "here here";
   //      ?>
   //  });
});
</script>

