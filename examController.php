<?php
	session_start();
	include("includes/inc_dbc.php");

	if(isset($_SESSION['user']) && $_SESSION['role']=="student")
	{
		if(isset($_POST['submitExamAnswers']))
		{
			$timeTaken = $_COOKIE['timeTaken'];
			$hidden = explode('&',$_POST['submitExamAnswers']);
			$eid = $hidden[0];
			$sid = $hidden[1];

			$total = $hidden[2];
			$correct = 0;
			$wrong = 0;
			$unanswered = 0;
			foreach ($_POST as $key => $value) 
			{
				if(is_int($key))
				{
					$sqla = "SELECT qid, correct FROM tbl_questions WHERE qid=".$key." AND correct='choice_".$value."'";
					$runa = mysqli_query($dbc, $sqla);
					$counta = mysqli_num_rows($runa);
					if($counta>0)
						$correct++;
					else
						$wrong++;
						//$score += $counta;
				}
			}

			$unanswered = $total - ($correct + $wrong);
			
			unset($_SESSION['examongoing']);
			setcookie('timeTaken','',1,'/');

			$sqlr = "INSERT INTO tbl_results(sid,eid,total_questions,correct,wrong,unanswered,timetaken) VALUES($sid,$eid,$total,$correct,$wrong,$unanswered,$timeTaken)";
			$runr = mysqli_query($dbc,$sqlr);

			$sqlat = "SELECT apply_for FROM tbl_users_students WHERE sid=".$sid;
			$runat = mysqli_query($dbc,$sqlat);
			$attended_exam="";
			while($countat = mysqli_fetch_array($runat))
			{
				$attended_exam = $countat['apply_for'];
			}

			$sqlrr = "UPDATE tbl_users_students SET score=".$correct.", apply_for='', attended_exam='".$attended_exam."' WHERE sid=".$sid;
			$runrr = mysqli_query($dbc,$sqlrr);
			
			if($runr AND $runrr)
			{
				$_SESSION['success'] = "You have successfully completed the examination.";
				header('Location: results.php');			
			}
			else
			{
				$_SESSION['error'] = "Something went wrong.";
				header('Location: results.php');
			}
		}

		else
		{
			header('Location: userIndex.php');
		}
	}

	else if(isset($_SESSION['user']) && ($_SESSION['role']=="teacher" OR $_SESSION['role']=="admin"))
	{
		if(isset($_POST['addQuestionsFromDoc']))
		{
			setcookie('radioCounter',0,time()-1000,'/');
			$goback = "addExams.php";
			$addDoc = $_FILES["add_doc"]["name"];

			$target_dir = "uploads/";
			if($addDoc!="")
			{
				$target_file = $target_dir . basename($_FILES["add_doc"]["name"]);
				$uploadOk = 1;
				$docFileType = pathinfo($target_file,PATHINFO_EXTENSION);

				// Allow certain file formats
				if($docFileType != "doc" && $docFileType != "docx" && $docFileType != "DOC" && $docFileType != "DOCX") 
				{
				    $_SESSION['error'] = "Sorry, only DOC & DOCX files are allowed.";
				    $uploadOk = 0;
				}
				
				// Check file size
				if ($_FILES["add_doc"]["size"] > 5000000) 
				{
				    $_SESSION['error'] = "Sorry, your file is too large.";
				    $uploadOk = 0;
				}

				$tmpfile = $_FILES["add_doc"]["tmp_name"];
				$mainfile = $_FILES["add_doc"]["name"];

				if($uploadOk != 0)
				{
					if (move_uploaded_file($tmpfile, $target_file)) 
					{
				        $_SESSION['doc_link'] = "uploads/".$addDoc;
				        $_SESSION['success'] = "Please select the correct answers for each questions added from the document.";
				        header("Location: $goback");		        
				    }
				    else 
				    {
				        $_SESSION['error'] = "Something went wrong while adding questions from the document.";
				        header("Location: $goback");
				    }
				}
				else
				{
		        	$_SESSION['error'] = "Something went wrong while adding questions from the document.";
					header("Location: $goback");
				}
			}
			else
			{
				header("Location: $goback");
			}
		}

		else if(isset($_POST['addExam']))
		{
			$errors = 0;
			$ecode = $_POST['ecode'];
			$ename = $_POST['ename'];
			$etype = $_POST['etype'];
			$edate = $_POST['edate'];
			$etime = $_POST['etime'];
			$hr = is_numeric($_POST['edurationh'])?($_POST['edurationh']):(0);
			$min = is_numeric($_POST['edurationm'])?($_POST['edurationm']):(0);
			$sec = is_numeric($_POST['edurations'])?($_POST['edurations']):(0);
			$eduration = $sec + ($min * 60) + ($hr * 60 * 60);
			$questions = (isset($_POST['questions']))?($_POST['questions']):"";
			$category = (isset($_POST['category']))?($_POST['category']):"";
			$choice_a = (isset($_POST['choice_a']))?($_POST['choice_a']):"";
			$choice_b = (isset($_POST['choice_b']))?($_POST['choice_b']):"";
			$choice_c = (isset($_POST['choice_c']))?($_POST['choice_c']):"";
			$choice_d = (isset($_POST['choice_d']))?($_POST['choice_d']):"";
			$correct = (isset($_POST['correct']))?($_POST['correct']):"";
			$order = (isset($_POST['questionsOrder']))?($_POST['questionsOrder']):"";
			$order = explode(",",str_replace("well_", "", $order));
			foreach ($order as $key => $value) {
				$actualorder[$value] = $key;
			}
			
			$sqle = "INSERT INTO tbl_exams(ecode,ename,etype,edate,etime,eduration,status) VALUES('$ecode','$ename','$etype','$edate','$etime','$eduration','active')";
			$rune = mysqli_query($dbc,$sqle);
			if($rune)
			{
				$sqls = "SELECT eid FROM tbl_exams WHERE ecode='$ecode'";
				$runs = mysqli_query($dbc,$sqls);
				if(($counts = mysqli_num_rows($runs))>0)
				{
					while($rows = mysqli_fetch_array($runs))
					{
						$eid = $rows['eid'];
					}
				}
				else
				{
					$errors++;
				}
			}
			else
			{
				$errors++;
			}
			if($questions!="" && $errors==0)
			{
				foreach ($questions as $key => $value) 
				{
					$sqlq = "INSERT INTO tbl_questions(eid,question,category,choice_a,choice_b,choice_c,choice_d,correct,qorder) VALUES('$eid','".$value."','".$category[$key]."','".$choice_a[$key]."','".$choice_b[$key]."','".$choice_c[$key]."','".$choice_d[$key]."','choice_".$correct[$key]."',".$actualorder[$key].")";
					$runq = mysqli_query($dbc,$sqlq);
					if($runq==0)
					{
						$errors++;
					}
				}
			}
			if($errors==0)
			{
				$_SESSION['success'] = "The exam was added successfully.";
				header('Location: exams.php');			
			}
			else
			{
				$_SESSION['error'] = "The exam could not be added.";
				header('Location: exams.php');	
			}
		}

		else if(isset($_POST['editExam']))
		{
			$errors = 0;
			$eid = $_POST['eid'];
			$ecode = $_POST['ecode'];
			$ename = $_POST['ename'];
			$etype = $_POST['etype'];
			$edate = $_POST['edate'];
			$etime = $_POST['etime'];
			$hr = is_numeric($_POST['edurationh'])?($_POST['edurationh']):(0);
			$min = is_numeric($_POST['edurationm'])?($_POST['edurationm']):(0);
			$sec = is_numeric($_POST['edurations'])?($_POST['edurations']):(0);
			$eduration = $sec + ($min * 60) + ($hr * 60 * 60);
			$qid = (isset($_POST['qid']))?($_POST['qid']):"";
			$questions = (isset($_POST['questions']))?($_POST['questions']):"";
			$category = (isset($_POST['category']))?($_POST['category']):"";
			$choice_a = (isset($_POST['choice_a']))?($_POST['choice_a']):"";
			$choice_b = (isset($_POST['choice_b']))?($_POST['choice_b']):"";
			$choice_c = (isset($_POST['choice_c']))?($_POST['choice_c']):"";
			$choice_d = (isset($_POST['choice_d']))?($_POST['choice_d']):"";
			$correct = (isset($_POST['correct']))?($_POST['correct']):"";
			$order = (isset($_POST['questionsOrder']))?($_POST['questionsOrder']):"";
			$order = explode(",",str_replace("well_", "", $order));
			foreach ($order as $key => $value) {
				$actualorder[$value] = $key;
			}
			
			// $sqle = "INSERT INTO tbl_exams(ecode,ename,etype,edate,etime,eduration,status) VALUES('$ecode','$ename','$etype','$edate','$etime','$eduration','active')";
			$sqle = "UPDATE tbl_exams SET ecode='$ecode', ename='$ename', etype='$etype', edate='$edate', etime='$etime', eduration='$eduration', status='active' WHERE eid=$eid";
			$rune = mysqli_query($dbc,$sqle);
			if($rune)
			{
				$sqld = "DELETE FROM tbl_questions WHERE eid=$eid";
				$rund = mysqli_query($dbc,$sqld);
				if(!$rund)
				{
					$errors++;
				}
			}
			else
			{
				$errors++;
			}
			if($questions!="" && $errors==0)
			{
				foreach ($questions as $key => $value) 
				{
					$sqlq = "INSERT INTO tbl_questions(eid,question,category,choice_a,choice_b,choice_c,choice_d,correct,qorder) VALUES('$eid','".$value."','".$category[$key]."','".$choice_a[$key]."','".$choice_b[$key]."','".$choice_c[$key]."','".$choice_d[$key]."','choice_".$correct[$key]."',".$actualorder[$key].")";
					$runq = mysqli_query($dbc,$sqlq);
					if($runq==0)
					{
						$errors++;
					}
				}
			}
			if($errors==0)
			{
				$_SESSION['success'] = "The exam was updated successfully.";
				header('Location: exams.php');
			}
			else
			{
				$_SESSION['error'] = "The exam could not be updated.";
				header('Location: editExams.php?eid=$eid');
			}
		}

		else if(isset($_GET['deleteThisExam']) && $_GET['deleteThisExam']!="")
		{
			$did = $_GET['deleteThisExam'];
			$sqldq = "DELETE FROM tbl_questions WHERE eid=$did";
			if(isset($_GET['reallyDeleteThisExam']))
				$sqlde = "DELETE FROM tbl_exams WHERE eid=$did";
			else
				$sqlde = "UPDATE tbl_exams SET status='inactive' WHERE eid=$did";
			
			$rundq = mysqli_query($dbc,$sqldq);
			$runde = mysqli_query($dbc,$sqlde);
			if($runde!=0 && $rundq!=0)
			{
				$_SESSION['success'] = "The exam was deleted successfully.";
				header('Location: exams.php');
			}
			else
			{
				$_SESSION['error'] = "The exam could not be deleted.";
				header('Location: exams.php');
			}
		}

		else
		{
			header('Location: userIndex.php');
		}
	}

	else
	{
		$_SESSION['error'] = "You must have gotten lost!";
		header('Location: index.php');
	}
?>