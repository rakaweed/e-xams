<?php
	session_start();
	include("includes/inc_dbc.php");

	$base_dir = "http://localhost/e-xams/";

	if(isset($_SESSION['user']) && ($_SESSION['role']=="teacher" OR $_SESSION['role']=="admin"))
	{
		/*CRUD Students*/
		if(isset($_POST['addStudent']))
		{
			$propic = $_FILES["spropic"]["name"];

			$target_dir = "uploads/propic/";
			if($propic!="")
			{
				$err = "";
				$target_file = $target_dir . basename($_FILES["spropic"]["name"]);
				$uploadOk = 1;
				$picFileType = pathinfo($target_file,PATHINFO_EXTENSION);

				// Allow certain file formats
				if($picFileType != "jpg" && $picFileType != "jpeg" && $picFileType != "png" && $picFileType != "gif" && $picFileType != "bmp" && $picFileType != "JPG" && $picFileType != "JPEG" && $picFileType != "PNG" && $picFileType != "GIF" && $picFileType != "BMP") 
				{
				    $err .= "<br>Only JPG, JPEG, BMP and GIF file formats are allowed.";
				    $uploadOk = 0;
				}
				
				// Check file size
				if ($_FILES["spropic"]["size"] > 500000) //Max Size: 500 KB 
				{
				    $err .= "<br>Your file is too large.";
				    $uploadOk = 0;
				}

				$tmpfile = $_FILES["spropic"]["tmp_name"];
				// $mainfile = $_FILES["spropic"]["name"];

				if($uploadOk != 0)
				{
					if (move_uploaded_file($tmpfile, $target_file)) 
					{
						$fname = $_POST['sfname'];
						$lname = $_POST['slname'];
						$gender = $_POST['sgender'];
						$address = $_POST['saddress'];
						$phone = $_POST['sphone'];
						$email = $_POST['semail'];
						$birthday = $_POST['sdob'];
						$grad_level = $_POST['sgradlevel'];
						$grad_year = $_POST['sgradyear'];
						$apply_for = $_POST['sapplyfor']; 
						$username = $_POST['susername']; 
						$password = $_POST['spassword'];

						$sqlu = "INSERT INTO tbl_users(username,password,role,status) VALUES('$username','$password','student','active')";
						$runu = mysqli_query($dbc, $sqlu);
						if($runu)
						{
							$sqlg = "SELECT uid FROM tbl_users WHERE username='$username' AND password='$password'";
							$rung = mysqli_query($dbc, $sqlg);
							$countg = mysqli_num_rows($rung);
							if($countg>0)
							{
								$rowg = mysqli_fetch_array($rung);
								$uid = $rowg['uid'];
								$propic = $base_dir.$target_dir.$propic;
								$sqls = "INSERT INTO tbl_users_students(uid,fname,lname,gender,address,phone,email,birthday,propic,grad_level,grad_year,apply_for) VALUES($uid,'$fname','$lname','$gender','$address','$phone','$email','$birthday','$propic','$grad_level','$grad_year','$apply_for')";
								$runs = mysqli_query($dbc, $sqls);

								if($runs)
								{
						        	$_SESSION['success'] = "The student was successfully added.";
						        	header("Location: students.php");
								}
								else
								{
									$_SESSION['error'] = "The student could not be registered at this time.";
						        	header("Location: students.php");
								}

							}
							else
							{
								$_SESSION['error'] = "The student could not be registered at this time.";
					        	header("Location: students.php");
							}
						}
						else
						{
							$_SESSION['error'] = "The student could not be registered at this time.";
				        	header("Location: students.php");
						}		        
				    }
				    else 
				    {
				        $_SESSION['error'] = "Image could not be uploaded.";
				        header("Location: students.php");
				    }
				}
				else
				{
					$_SESSION['error'] = "Sorry, the following error(s) occured:".$err;
					header("Location: students.php");
				}
			}
			else
			{
				header("Location: students.php");
			}
		}

		else if(isset($_POST['editStudent']))
		{
			$sid = $_POST['sid'];
			$uid = $_POST['uid'];
			$fname = $_POST['sfname'];
			$lname = $_POST['slname'];
			$gender = $_POST['sgender'];
			$address = $_POST['saddress'];
			$phone = $_POST['sphone'];
			$email = $_POST['semail'];
			$birthday = $_POST['sdob'];
			$grad_level = $_POST['sgradlevel'];
			$grad_year = $_POST['sgradyear'];
			$apply_for = $_POST['sapplyfor']; 
			$attended_exam = isset($_POST['attended_exam'])?$_POST['sapplyfor']:"";
			$username = $_POST['susername']; 
			$password = $_POST['spassword'];

			$propic = $_FILES["spropic"]["name"];

			$target_dir = "uploads/propic/";
			
			$err = "";

			if($propic!="")
			{
				$target_file = $target_dir . basename($_FILES["spropic"]["name"]);
				$uploadOk = 1;
				$picFileType = pathinfo($target_file,PATHINFO_EXTENSION);

				// Allow certain file formats
				if($picFileType != "jpg" && $picFileType != "jpeg" && $picFileType != "png" && $picFileType != "gif" && $picFileType != "bmp" && $picFileType != "JPG" && $picFileType != "JPEG" && $picFileType != "PNG" && $picFileType != "GIF" && $picFileType != "BMP") 
				{
				    $err .= "<br>Only JPG, JPEG, BMP and GIF file formats are allowed.";
				    $uploadOk = 0;
				}
				
				// Check file size
				if ($_FILES["spropic"]["size"] > 500000) //Max Size: 500 KB 
				{
				    $err .= "<br>Your file is too large.";
				    $uploadOk = 0;
				}

				$tmpfile = $_FILES["spropic"]["tmp_name"];
				// $mainfile = $_FILES["spropic"]["name"];

				if($uploadOk != 0)
				{
					if (move_uploaded_file($tmpfile, $target_file)) 
					{
						$propic = $base_dir.$target_dir.$propic;				        
				    }
				    else 
				    {
				        $propic = "";
				    }
				}
				else
				{
					$_SESSION['error'] = "Sorry, the following error(s) occured:".$err;
					header("Location: editStudents.php?sid=".$sid);
				}
			}
			else
			{
				$propic = "";
			}

			$sqlu = "UPDATE tbl_users SET username='$username', password='$password', status='active' WHERE uid=$uid";
			$sqls = ($propic!="")?("UPDATE tbl_users_students SET fname='$fname', lname='$lname', gender='$gender', address='$address', phone='$phone', email='$email', birthday='$birthday', propic='$propic', grad_level='$grad_level', grad_year='$grad_year', apply_for='$apply_for', attended_exam='$attended_exam' WHERE sid=$sid"):("UPDATE tbl_users_students SET fname='$fname', lname='$lname', gender='$gender', address='$address', phone='$phone', email='$email', birthday='$birthday', grad_level='$grad_level', grad_year='$grad_year', apply_for='$apply_for', attended_exam='$attended_exam' WHERE sid=$sid");
			
			$runu = mysqli_query($dbc, $sqlu);
			$runs = mysqli_query($dbc, $sqls);

			if($runu && $runs)
			{
	        	$_SESSION['success'] = "The student was successfully updated.";
	        	header("Location: students.php");
			}
			else
			{
	        	$_SESSION['error'] = "The student could not be updated.";
	        	header("Location: students.php");
			}
		}

		else if(isset($_GET['deleteThisStudent']) && $_GET['deleteThisStudent']!="")
		{
			$did = $_GET['deleteThisStudent'];
			$sqls = "SELECT uid FROM tbl_users_students WHERE sid=$did";
			$runs = mysqli_query($dbc,$sqls);
			$counts = mysqli_num_rows($runs);
			if($counts>0)
			{
				$rows = mysqli_fetch_array($runs);
				$uid = $rows['uid'];

				// $sqlds = "DELETE FROM tbl_users_students WHERE sid=$did";
				$sqldu = "UPDATE tbl_users SET status='inactive' WHERE uid=$uid";

				// $runds = mysqli_query($dbc,$sqlds);
				$rundu = mysqli_query($dbc,$sqldu);
				if($rundu!=0)
				{
					$_SESSION['success'] = "The student was deleted successfully.";
					header('Location: students.php');
				}
				else
				{
					$_SESSION['error'] = "The student could not be deleted.";
					header('Location: students.php');
				}
			}
			else
			{
				$_SESSION['error'] = "The student does not exist.";
				header("Location: students.php");
			}
		}
		/*CRUD Students*/

		else if($_SESSION['role']=='admin')
		{
			/*CRUD Teachers*/
			if(isset($_POST['addTeacher']))
			{
				$propic = $_FILES["tpropic"]["name"];

				$target_dir = "uploads/propic/";
				if($propic!="")
				{
					$err = "";
					$target_file = $target_dir . basename($_FILES["tpropic"]["name"]);
					$uploadOk = 1;
					$picFileType = pathinfo($target_file,PATHINFO_EXTENSION);

					// Allow certain file formats
					if($picFileType != "jpg" && $picFileType != "jpeg" && $picFileType != "png" && $picFileType != "gif" && $picFileType != "bmp" && $picFileType != "JPG" && $picFileType != "JPEG" && $picFileType != "PNG" && $picFileType != "GIF" && $picFileType != "BMP") 
					{
					    $err .= "<br>Only JPG, JPEG, BMP and GIF file formats are allowed.";
					    $uploadOk = 0;
					}
					
					// Check file size
					if ($_FILES["tpropic"]["size"] > 500000) //Max Size: 500 KB 
					{
					    $err .= "<br>Your file is too large.";
					    $uploadOk = 0;
					}

					$tmpfile = $_FILES["tpropic"]["tmp_name"];
					// $mainfile = $_FILES["spropic"]["name"];

					if($uploadOk != 0)
					{
						if (move_uploaded_file($tmpfile, $target_file)) 
						{
							$fname = $_POST['tfname'];
							$lname = $_POST['tlname'];
							$gender = $_POST['tgender'];
							$address = $_POST['taddress'];
							$phone = $_POST['tphone'];
							$email = $_POST['temail'];
							$faculty = $_POST['tfaculty'];
							$post = $_POST['tpost'];
							$username = $_POST['tusername']; 
							$password = $_POST['tpassword'];

							$sqlu = "INSERT INTO tbl_users(username,password,role,status) VALUES('$username','$password','teacher','active')";
							$runu = mysqli_query($dbc, $sqlu);
							if($runu)
							{
								$sqlg = "SELECT uid FROM tbl_users WHERE username='$username' AND password='$password'";
								$rung = mysqli_query($dbc, $sqlg);
								$countg = mysqli_num_rows($rung);
								if($countg>0)
								{
									$rowg = mysqli_fetch_array($rung);
									$uid = $rowg['uid'];
									$propic = $base_dir.$target_dir.$propic;
									$sqlt = "INSERT INTO tbl_users_admins(uid,fname,lname,gender,address,phone,email,faculty,post,propic) VALUES($uid,'$fname','$lname','$gender','$address','$phone','$email','$faculty','$post','$propic')";
									$runt = mysqli_query($dbc, $sqlt);

									if($runt)
									{
							        	$_SESSION['success'] = "The teacher was successfully added.";
							        	header("Location: teachers.php");
									}
									else
									{
										$_SESSION['error'] = "The teacher could not be registered at this time.";
							        	header("Location: teachers.php");
									}
								}
								else
								{
									$_SESSION['error'] = "The teacher could not be registered at this time.";
						        	header("Location: teachers.php");
								}
							}
							else
							{
								$_SESSION['error'] = "The teacher could not be registered at this time.";
					        	header("Location: teachers.php");
							}		        
					    }
					    else 
					    {
					        $_SESSION['error'] = "Image could not be uploaded.";
					        header("Location: teachers.php");
					    }
					}
					else
					{
						$_SESSION['error'] = "Sorry, the following error(s) occured:".$err;
						header("Location: teachers.php");
					}
				}
				else
				{
					header("Location: teachers.php");
				}
			}

			else if(isset($_POST['editTeacher']))
			{
				$tid = $_POST['tid'];
				$uid = $_POST['uid'];
				$fname = $_POST['tfname'];
				$lname = $_POST['tlname'];
				$gender = $_POST['tgender'];
				$address = $_POST['taddress'];
				$phone = $_POST['tphone'];
				$email = $_POST['temail'];
				$faculty = $_POST['tfaculty'];
				$post = $_POST['tpost'];
				$username = $_POST['tusername']; 
				$password = $_POST['tpassword'];

				$propic = $_FILES["tpropic"]["name"];

				$target_dir = "uploads/propic/";
				
				$err = "";

				if($propic!="")
				{
					$target_file = $target_dir . basename($_FILES["tpropic"]["name"]);
					$uploadOk = 1;
					$picFileType = pathinfo($target_file,PATHINFO_EXTENSION);

					// Allow certain file formats
					if($picFileType != "jpg" && $picFileType != "jpeg" && $picFileType != "png" && $picFileType != "gif" && $picFileType != "bmp" && $picFileType != "JPG" && $picFileType != "JPEG" && $picFileType != "PNG" && $picFileType != "GIF" && $picFileType != "BMP") 
					{
					    $err .= "<br>Only JPG, JPEG, BMP and GIF file formats are allowed.";
					    $uploadOk = 0;
					}
					
					// Check file size
					if ($_FILES["tpropic"]["size"] > 500000) //Max Size: 500 KB 
					{
					    $err .= "<br>Your file is too large.";
					    $uploadOk = 0;
					}

					$tmpfile = $_FILES["tpropic"]["tmp_name"];
					// $mainfile = $_FILES["spropic"]["name"];

					if($uploadOk != 0)
					{
						if (move_uploaded_file($tmpfile, $target_file)) 
						{
							$propic = $base_dir.$target_dir.$propic;				        
					    }
					    else 
					    {
					        $propic = "";
					    }
					}
					else
					{
						$_SESSION['error'] = "Sorry, the following error(s) occured:".$err;
						header("Location: editTeachers.php?tid=".$tid);
					}
				}
				else
				{
					$propic = "";
				}

				$sqlu = "UPDATE tbl_users SET username='$username', password='$password', status='active' WHERE uid=$uid";
				$sqlt = ($propic!="")?("UPDATE tbl_users_admins SET fname='$fname', lname='$lname', gender='$gender', address='$address', phone='$phone', email='$email', faculty='$faculty', post='$post', propic='$propic' WHERE aid=$tid"):("UPDATE tbl_users_admins SET fname='$fname', lname='$lname', gender='$gender', address='$address', phone='$phone', email='$email', faculty='$faculty', post='$post' WHERE aid=$tid");
				
				$runu = mysqli_query($dbc, $sqlu);
				$runt = mysqli_query($dbc, $sqlt);

				if($runu && $runt)
				{
		        	$_SESSION['success'] = "The teacher was successfully updated.";
		        	header("Location: teachers.php");
				}
				else
				{
		        	$_SESSION['error'] = "The teacher could not be updated.";
		        	header("Location: teachers.php");
				}
			}

			else if(isset($_GET['deleteThisTeacher']) && $_GET['deleteThisTeacher']!="")
			{
				$did = $_GET['deleteThisTeacher'];
				$sqlt = "SELECT uid FROM tbl_users_admins WHERE aid=$did";
				$runt = mysqli_query($dbc,$sqlt);
				$countt = mysqli_num_rows($runt);
				if($countt>0)
				{
					$rowt = mysqli_fetch_array($runt);
					$uid = $rowt['uid'];

					$sqldu = "DELETE FROM tbl_users WHERE uid=$uid";
					$sqldt = "DELETE FROM tbl_users_admins WHERE aid=$did";

					// $runds = mysqli_query($dbc,$sqlds);
					$rundu = mysqli_query($dbc,$sqldu);
					$rundt = mysqli_query($dbc,$sqldt);

					if($rundu && $rundt)
					{
						$_SESSION['success'] = "The teacher was deleted successfully.";
						header('Location: teachers.php');
					}
					else
					{
						$_SESSION['error'] = "The teacher could not be deleted.";
						header('Location: teachers.php');
					}
				}
				else
				{
					$_SESSION['error'] = "The teacher does not exist.";
					header("Location: teachers.php");
				}
			}
			/*CRUD Teachers*/

			/*CRUD Admin*/
			else if(isset($_POST['addAdmin']))
			{
				$propic = $_FILES["apropic"]["name"];

				$target_dir = "uploads/propic/";
				if($propic!="")
				{
					$err = "";
					$target_file = $target_dir . basename($_FILES["apropic"]["name"]);
					$uploadOk = 1;
					$picFileType = pathinfo($target_file,PATHINFO_EXTENSION);

					// Allow certain file formats
					if($picFileType != "jpg" && $picFileType != "jpeg" && $picFileType != "png" && $picFileType != "gif" && $picFileType != "bmp" && $picFileType != "JPG" && $picFileType != "JPEG" && $picFileType != "PNG" && $picFileType != "GIF" && $picFileType != "BMP") 
					{
					    $err .= "<br>Only JPG, JPEG, BMP and GIF file formats are allowed.";
					    $uploadOk = 0;
					}
					
					// Check file size
					if ($_FILES["apropic"]["size"] > 500000) //Max Size: 500 KB 
					{
					    $err .= "<br>Your file is too large.";
					    $uploadOk = 0;
					}

					$tmpfile = $_FILES["apropic"]["tmp_name"];
					// $mainfile = $_FILES["spropic"]["name"];

					if($uploadOk != 0)
					{
						if (move_uploaded_file($tmpfile, $target_file)) 
						{
							$fname = $_POST['afname'];
							$lname = $_POST['alname'];
							$gender = $_POST['agender'];
							$address = $_POST['aaddress'];
							$phone = $_POST['aphone'];
							$email = $_POST['aemail'];
							$faculty = $_POST['afaculty'];
							$post = $_POST['apost'];
							$username = $_POST['ausername']; 
							$password = $_POST['apassword'];

							$sqlu = "INSERT INTO tbl_users(username,password,role,status) VALUES('$username','$password','admin','active')";
							$runu = mysqli_query($dbc, $sqlu);
							if($runu)
							{
								$sqlg = "SELECT uid FROM tbl_users WHERE username='$username' AND password='$password'";
								$rung = mysqli_query($dbc, $sqlg);
								$countg = mysqli_num_rows($rung);
								if($countg>0)
								{
									$rowg = mysqli_fetch_array($rung);
									$uid = $rowg['uid'];
									$propic = $base_dir.$target_dir.$propic;
									$sqla = "INSERT INTO tbl_users_admins(uid,fname,lname,gender,address,phone,email,faculty,post,propic) VALUES($uid,'$fname','$lname','$gender','$address','$phone','$email','$faculty','$post','$propic')";
									$runa = mysqli_query($dbc, $sqla);

									if($runa)
									{
							        	$_SESSION['success'] = "The admin was successfully added.";
							        	header("Location: admins.php");
									}
									else
									{
										$_SESSION['error'] = "The admin could not be registered at this time.";
							        	header("Location: admins.php");
									}
								}
								else
								{
									$_SESSION['error'] = "The admin could not be registered at this time.";
						        	header("Location: admins.php");
								}
							}
							else
							{
								$_SESSION['error'] = "The admin could not be registered at this time.";
					        	header("Location: admins.php");
							}		        
					    }
					    else 
					    {
					        $_SESSION['error'] = "Image could not be uploaded.";
					        header("Location: admins.php");
					    }
					}
					else
					{
						$_SESSION['error'] = "Sorry, the following error(s) occured:".$err;
						header("Location: admins.php");
					}
				}
				else
				{
					header("Location: admins.php");
				}
			}

			else if(isset($_POST['editAdmin']))
			{
				$aid = $_POST['aid'];
				$uid = $_POST['uid'];
				$fname = $_POST['afname'];
				$lname = $_POST['alname'];
				$gender = $_POST['agender'];
				$address = $_POST['aaddress'];
				$phone = $_POST['aphone'];
				$email = $_POST['aemail'];
				$faculty = $_POST['afaculty'];
				$post = $_POST['apost'];
				$username = $_POST['ausername']; 
				$password = $_POST['apassword'];

				$propic = $_FILES["apropic"]["name"];

				$target_dir = "uploads/propic/";
				
				$err = "";

				if($propic!="")
				{
					$target_file = $target_dir . basename($_FILES["apropic"]["name"]);
					$uploadOk = 1;
					$picFileType = pathinfo($target_file,PATHINFO_EXTENSION);

					// Allow certain file formats
					if($picFileType != "jpg" && $picFileType != "jpeg" && $picFileType != "png" && $picFileType != "gif" && $picFileType != "bmp" && $picFileType != "JPG" && $picFileType != "JPEG" && $picFileType != "PNG" && $picFileType != "GIF" && $picFileType != "BMP") 
					{
					    $err .= "<br>Only JPG, JPEG, BMP and GIF file formats are allowed.";
					    $uploadOk = 0;
					}
					
					// Check file size
					if ($_FILES["apropic"]["size"] > 500000) //Max Size: 500 KB 
					{
					    $err .= "<br>Your file is too large.";
					    $uploadOk = 0;
					}

					$tmpfile = $_FILES["apropic"]["tmp_name"];
					// $mainfile = $_FILES["spropic"]["name"];

					if($uploadOk != 0)
					{
						if (move_uploaded_file($tmpfile, $target_file)) 
						{
							$propic = $base_dir.$target_dir.$propic;				        
					    }
					    else 
					    {
					        $propic = "";
					    }
					}
					else
					{
						$_SESSION['error'] = "Sorry, the following error(s) occured:".$err;
						header("Location: editAdmins.php?tid=".$tid);
					}
				}
				else
				{
					$propic = "";
				}

				$sqlu = "UPDATE tbl_users SET username='$username', password='$password', status='active' WHERE uid=$uid";
				$sqla = ($propic!="")?("UPDATE tbl_users_admins SET fname='$fname', lname='$lname', gender='$gender', address='$address', phone='$phone', email='$email', faculty='$faculty', post='$post', propic='$propic' WHERE aid=$aid"):("UPDATE tbl_users_admins SET fname='$fname', lname='$lname', gender='$gender', address='$address', phone='$phone', email='$email', faculty='$faculty', post='$post' WHERE aid=$aid");
				
				$runu = mysqli_query($dbc, $sqlu);
				$runa = mysqli_query($dbc, $sqla);

				if($runu && $runa)
				{
		        	$_SESSION['success'] = "The admin was successfully updated.";
		        	header("Location: admins.php");
				}
				else
				{
		        	$_SESSION['error'] = "The admin could not be updated.";
		        	header("Location: admins.php");
				}
			}

			else if(isset($_GET['deleteThisAdmin']) && $_GET['deleteThisAdmin']!="")
			{
				$did = $_GET['deleteThisAdmin'];
				$sqla = "SELECT uid FROM tbl_users_admins WHERE aid=$did";
				$runa = mysqli_query($dbc,$sqla);
				$counta = mysqli_num_rows($runa);
				if($counta>0)
				{
					$rowa = mysqli_fetch_array($runa);
					$uid = $rowa['uid'];

					$sqldu = "DELETE FROM tbl_users WHERE uid=$uid";
					$sqlda = "DELETE FROM tbl_users_admins WHERE aid=$did";

					$rundu = mysqli_query($dbc,$sqldu);
					$runda = mysqli_query($dbc,$sqlda);

					if($rundu && $runda)
					{
						$_SESSION['success'] = "The admin was deleted successfully.";
						header('Location: admins.php');
					}
					else
					{
						$_SESSION['error'] = "The admin could not be deleted.";
						header('Location: admins.php');
					}
				}
				else
				{
					$_SESSION['error'] = "The admin does not exist.";
					header("Location: admins.php");
				}
			}
			/*CRUD Admin*/

			/*CRUD Gallery/Images*/
			else if(isset($_POST['addInGallery']))
			{
				$errors = 0;
				$img_name = array_values($_POST['img_name']);

				function picFilesArray( $arr )
				{
				    foreach( $arr as $key => $all )
				    {
				        foreach( $all as $i => $val )
				        {
				            $new[$i][$key] = $val;    
				        }    
				    }
				    return $new;
				}

    			$file_array = picFilesArray($_FILES['location_file']);
    			$file_array = array_values($file_array);

				$uploadOk = 1;
				$err = "";
				$target_dir = "uploads/img_gallery/";
				foreach($file_array as $file)
				{
					$file_name_temp = $file['name'];
					$target_file_temp = $target_dir . basename($file["name"]);
					$file_name[] = $file_name_temp;
					$target_file[] = $target_file_temp;
					$imgFileType = pathinfo($target_file_temp,PATHINFO_EXTENSION);

					if($imgFileType != "jpg" && $imgFileType != "jpeg" && $imgFileType != "png" && $imgFileType != "bmp" && $imgFileType != "JPG" && $imgFileType != "JPEG" && $imgFileType != "PNG" && $imgFileType != "BMP") 
					{
					    $err .= "<br>Image '".$file_name_temp."' is not of type JPG, JPEG, PNG or BMP.";
					    $uploadOk = 0;
					}
					
					// Check file size
					if ($file["size"] > 2000000) 
					{
					    $err .= "<br>File size of '".$file_name_temp."' is too large.";
					    $uploadOk = 0;
					}

					$tmpfile[] = $file["tmp_name"];
				}

				$status = array_values($_POST['status']);

				if($uploadOk != 0)
				{
					foreach ($target_file as $key => $tfile)
					{
					// 	echo "<br>".$tmpfile[$key]." , ".$target_file[$key];continue;
						$up = move_uploaded_file($tmpfile[$key],$target_file[$key]);
						if(!$up)
						{
							$err .= "<br>Image '".$file_name[$key]."' could not be uploaded.";
							$uploadOk = 0;
						}
					}
				}

				if($uploadOk != 0)
				{
					$sqlo = "SELECT MAX(appear_order) as ap_or FROM tbl_gallery";
					$runo = mysqli_query($dbc,$sqlo);
					if(mysqli_num_rows($runo)!=NULL)
					{
						$rowo = mysqli_fetch_array($runo);
						$actualorder = $rowo['ap_or']?$rowo['ap_or']+1:0;
					}
					
					foreach ($img_name as $key => $value)
					{
						$sqli = "INSERT INTO tbl_gallery(img_name,location,appear_order,status) VALUES('$value','".$file_name[$key]."','".$actualorder."','".$status[$key]."')";
						$runi = mysqli_query($dbc,$sqli);
						if($runi==0)
						{
							$errors++;
						}
						$actualorder++;
					}
				}
				else
				{
					$_SESSION['error'] = "Upload of files was prevented due to the following errors:".$err;
					header("Location: gallery.php");
				}

				if($errors==0)
				{
					$_SESSION['success'] = "Images were successfully added to the gallery.";
					header("Location: gallery.php");
				}	
				else
				{
					$_SESSION['error'] = $errors." images could not be added to the gallery.";
					header("Location: gallery.php");
				}
			}

			else if (isset($_GET['changestatusto']) && isset($_GET['img_id']) && $_GET['changestatusto']!="" && $_GET['img_id']!="") 
			{
				$sqls = "UPDATE tbl_gallery SET status='".$_GET['changestatusto']."' WHERE imgid=".$_GET['img_id'];
				$runs = mysqli_query($dbc,$sqls);
				$done = ($_GET['changestatusto']=="active")?"activated":"deactivated";
				
				if($runs)
				{
					$_SESSION['success'] = "The image has been ".$done;
					header("Location: gallery.php");
				}
				else
				{
					$_SESSION['error'] = "The image could not be ".$done;
					header("Location: gallery.php");
				}
			}

			else if (isset($_GET['deleteThisimgid']) && $_GET['deleteThisimgid']!="") 
			{
				$sqli = "DELETE FROM tbl_gallery WHERE imgid=".$_GET['deleteThisimgid'];
				$runi = mysqli_query($dbc,$sqli);
				
				if($runi)
				{
					$_SESSION['success'] = "The image has been deleted.";
					header("Location: gallery.php");
				}
				else
				{
					$_SESSION['error'] = "The image could not be deleted.";
					header("Location: gallery.php");
				}
			}

			else if(isset($_POST['saveImgOrder']))
			{
				$allids = $_POST['imgid'];
				$ao = 0;
				$error = 0;
				// echo "<pre>".print_r($allids,TRUE)."</pre>";exit;
				foreach ($allids as $imgid) 
				{
					$sqlu = "UPDATE tbl_gallery SET appear_order=$ao WHERE imgid=$imgid";
					$runu = mysqli_query($dbc,$sqlu);
					if(!$runu)
					{
						$error++;
					}
					$ao++;
				}

				if($error==0)
				{
					$_SESSION['success'] = "Image order has been changed successfully.";
					header("Location: gallery.php");
				}
				else
				{
					$_SESSION['error'] = "Image order could not be changed.";
					header("Location: gallery.php");
				}
			}
			/*CRUD Gallery/Images*/

			else
			{
				header("Location: userIndex.php");
			}
		}
		else
		{
			header("Location: userIndex.php");
		}
	}

	else if(isset($_POST['login-submit']))
	{
		$uname = $_POST['uname'];
		$passw = $_POST['passw'];

		$sql = "SELECT * FROM tbl_users WHERE username='".$uname."' AND password='".$passw."' AND status='active' ORDER BY uid";
		$run = mysqli_query($dbc,$sql);
		
		while($row = mysqli_fetch_array($run))
		{
			$_SESSION['user'] = $row['uid'];
			$_SESSION['role'] = $row['role'];
			header("Location: userIndex.php?".$_SESSION['role']);
		}

		if(!isset($_SESSION['user']))
		{
			$_SESSION['error'] = "Username or Password invalid.";
			header("Location: index.php");
		}
	}
	
	else
	{
		$_SESSION['error'] = "You must have gotten lost!";
		header('Location: index.php');
	}
?>