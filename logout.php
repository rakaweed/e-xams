<?php

session_start();
session_destroy();

foreach ($_COOKIE as $co => $kie) {
	setcookie($co, "", time()-3600);
}

session_start();
$_SESSION['success'] = "You have been logged out successfully.";
header("Location: index.php");

?>